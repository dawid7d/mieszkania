package com.example.mieszkania.mieszkania.Responses;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetUserReservationsResponse {
    boolean success;

    Data data;

    public static class Data {

        public Reservations[] getReservations() {
            return reservations;
        }

        public void setReservations(Reservations[] reservations) {
            this.reservations = reservations;
        }

        Reservations[] reservations;

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Reservations {
            private int user_id;
            private int advertisement_id;
            private String start_date;

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public int getAdvertisement_id() {
                return advertisement_id;
            }

            public void setAdvertisement_id(int advertisement_id) {
                this.advertisement_id = advertisement_id;
            }

            public String getStart_date() {
                return start_date;
            }

            public void setStart_date(String start_date) {
                this.start_date = start_date;
            }
        }



    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
