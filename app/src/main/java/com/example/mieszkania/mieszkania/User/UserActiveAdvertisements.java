package com.example.mieszkania.mieszkania.User;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.Flat;
import com.example.mieszkania.mieszkania.R;
import com.example.mieszkania.mieszkania.Requests.PutDeactivateAdvertisementRequest;
import com.example.mieszkania.mieszkania.Responses.AdvertisementsResponse;
import com.example.mieszkania.mieszkania.Responses.FlatByIdResponse;
import com.example.mieszkania.mieszkania.Responses.GetRoomByIdResponse;
import com.example.mieszkania.mieszkania.Responses.PutDeactivateAdvertisementResponse;
import com.example.mieszkania.mieszkania.Room;
import com.example.mieszkania.mieszkania.Tasks.AdvertisementsTask;
import com.example.mieszkania.mieszkania.Tasks.GetFlatByIdTask;
import com.example.mieszkania.mieszkania.Tasks.GetRoomByIdTask;
import com.example.mieszkania.mieszkania.Tasks.PutDeactivateAdvertisementTask;
import com.example.mieszkania.mieszkania.Tasks.UserActiveAdvertisementsTask;

import java.util.ArrayList;

public class UserActiveAdvertisements extends AppCompatActivity {

    static ArrayList<AdvertisementsResponse.Data.Advertisements> advertisements_list;
    ListView listView2;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_active_advertisements);

        advertisements_list = new ArrayList<>();
        AdvertisementsResponse advertisementsResponse = UserActiveAdvertisementsTask.unpack(new UserActiveAdvertisementsTask().execute());
        for(AdvertisementsResponse.Data.Advertisements a : advertisementsResponse.getData().getAdvertisement()){
            Log.v("ave","id=" + a.getId()+
                    " user id=" + a.getUser_id()+
                    " target id= " + a.getTarget_id()+
                    " target type= " + a.getTarget_type()+
                    " rental_price" + a.getRental_price()+
                    " start_data" + a.getStart_rent_data()+
                    " end_data" + a.getEnd_rent_data()+
                    " contract type" + a.getContract_type());
            advertisements_list.add(a);
        }

        AdvertisementsAdapter advertisementsAdapter = new AdvertisementsAdapter(advertisements_list,listView2);
        listView2 = findViewById(R.id.advertisements_list);
        listView2.setAdapter(advertisementsAdapter);
    }

    public class AdvertisementsAdapter extends BaseAdapter {
        ArrayList<AdvertisementsResponse.Data.Advertisements> advertisements_list;
        ListView listView2;

        AdvertisementsAdapter(ArrayList<AdvertisementsResponse.Data.Advertisements> _advertisements_list,ListView listView2){
            this.advertisements_list = _advertisements_list;
            this.listView2 = listView2;
        }

        @Override
        public int getCount() {
            return advertisements_list.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.user_active_advert_layout,null);

            TextView textView = view.findViewById(R.id.textView);
            String ad_active = "";
            if(advertisements_list.get(i).isIs_active()){ad_active="Tak";}
            else {ad_active="Nie";}
            textView.setText(" \n"+advertisements_list.get(i).getTitle()+"\n\n"+
                    " Koszt miesieczny: "+advertisements_list.get(i).getRental_price()+"zł \n"+
                    " Zaliczka: "+advertisements_list.get(i).getDeposit()+"zł\n\n"+
                    " Czy aktywne: "+ad_active+"\n\n"+
                    " Od: "+advertisements_list.get(i).getStart_rent_data()+"\n"+
                    " Do: "+advertisements_list.get(i).getEnd_rent_data()+"\n"
            );

            Button delete = view.findViewById(R.id.delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.v("$$","delete adevert : " +  advertisements_list.get(i).getId() );
                    PutDeactivateAdvertisementResponse response = PutDeactivateAdvertisementTask.unpack(new PutDeactivateAdvertisementTask().execute(advertisements_list.get(i).getId()));
                    notifyDataSetChanged();
                }
            });

            Button go_to_advert = view.findViewById(R.id.go_to_advert);
            go_to_advert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(advertisements_list.get(i).getTarget_type().equals("Flat")){
                        Log.v("ave", "flat target id to GET="+advertisements_list.get(i).getTarget_id());
                        int selectedFlatId = advertisements_list.get(i).getTarget_id();
                        FlatByIdResponse flatByIdResponse = GetFlatByIdTask.unpack(new GetFlatByIdTask().execute(new Integer(selectedFlatId)));

                        Intent intent = new Intent(UserActiveAdvertisements.this,Flat.class);
                        UserState.setCanEdit(false);
                        intent.putExtra("can_edit", UserState.getCanEdit());
                        intent.putExtra("Flat", flatByIdResponse.getData().getFlat());
                        intent.putExtra("Advertisement", advertisements_list.get(i));
                        startActivity(intent);
                    } else {
                        Log.v("ave", "room target id to GET="+advertisements_list.get(i).getTarget_id());
                        int selectedRoomId = advertisements_list.get(i).getTarget_id();
                        GetRoomByIdResponse RoomByIdResponse = GetRoomByIdTask.unpack(new GetRoomByIdTask().execute(new Integer(selectedRoomId)));
                        GetRoomByIdResponse.Data.Room room = RoomByIdResponse.getData().getRoom();
                        Intent intent = new Intent(UserActiveAdvertisements.this,Room.class);
                        UserState.setCanEdit(false);
                        intent.putExtra("can_edit", UserState.getCanEdit());
                        intent.putExtra("RoomById", RoomByIdResponse.getData().getRoom());
                        intent.putExtra("flat_id", RoomByIdResponse.getData().getRoom().getFlat_id());
                        startActivity(intent);
                    }
                }
            });

            return view;
        }
    }

}
