package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostAddRoomResponse {
    PostAddRoomResponse(){
        data = new Data();
        success = false;
    }
    boolean success;
    Data data;

    public Data getData() {
        return data;
    }

    public boolean isSuccess() {
        return success;
    }

    public static class Data{
        public Data(){
            room = new Room();
        }
        public static class Room{
            public Room(){
                room_type = new String();
                yardage = new Double(0.0);
                total_places = new Integer(0);
                can_rent_separately = false;
                id = new Integer(0);
            }
            Integer id;
            String room_type;
            Double yardage;
            Integer total_places;
            boolean can_rent_separately;
            String flat_id;
            String available_places;
            String created_at;
            String updated_at;

            public Double getYardage() {
                return yardage;
            }

            public Integer getTotal_places() {
                return total_places;
            }

            public String getRoom_type() {
                return room_type;
            }

            public boolean isCan_rent_separately() {
                return can_rent_separately;
            }

            public void setYardage(Double yardage) {
                this.yardage = yardage;
            }

            public void setCan_rent_separately(boolean can_rent_separately) {
                this.can_rent_separately = can_rent_separately;
            }

            public void setRoom_type(String room_type) {
                this.room_type = room_type;
            }

            public void setTotal_places(Integer total_places) {
                this.total_places = total_places;
            }

            public Integer getId() {
                return id;
            }
        }

        public Room getRoom() {
            return room;
        }

        Room room;
    }

}
