package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.AdvertisementsResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class UserActiveAdvertisementsTask extends AsyncTask<Void, Void, ResponseEntity<AdvertisementsResponse>> {

    @Override
    protected ResponseEntity<AdvertisementsResponse> doInBackground(Void... voids) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetAdvertisementTask", "sending GetAdvertisementTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getAdvertisementsEndpoint() + "/active";
        ResponseEntity<AdvertisementsResponse> getAdvertisementResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                AdvertisementsResponse.class);
        return getAdvertisementResponseEntity;
    }

    public static AdvertisementsResponse unpack(AsyncTask<Void, Void, ResponseEntity<AdvertisementsResponse>>  getAdvertisementResponseEntity)
    {
        try{
            ResponseEntity<AdvertisementsResponse> responseEntity = getAdvertisementResponseEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                AdvertisementsResponse AdvertisementsResponse  = responseEntity.getBody();
                if(AdvertisementsResponse != null) {
                    return AdvertisementsResponse;
                }
                else {
                    Log.v("GetAdvertisementTask", "AdvertisementsResponse == null");
                    return null;
                }
            }
            else{
                Log.v("GetAdvertisementTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetAdvertisementTask", e.toString());
        }
        return null;
    }
}
