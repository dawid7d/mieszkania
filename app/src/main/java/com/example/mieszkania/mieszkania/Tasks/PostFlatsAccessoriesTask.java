package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Requests.PostFlatsAccessoriesRequest;
import com.example.mieszkania.mieszkania.Requests.PostFlatsRequest;
import com.example.mieszkania.mieszkania.Responses.PostFlatsAccessoriesResponse;
import com.example.mieszkania.mieszkania.Responses.PostFlatsResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;



public class PostFlatsAccessoriesTask extends AsyncTask<PostFlatsAccessoriesTask.ParamStruct, Void, ResponseEntity<PostFlatsAccessoriesResponse>> {

    @Override
    protected ResponseEntity<PostFlatsAccessoriesResponse> doInBackground(ParamStruct ... params) {
        ParamStruct input = params[0];
        PostFlatsAccessoriesRequest objToPass = input.getPostFlatsAccessoriesRequest();
        Integer flatId = input.getFlatId();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("PostFlatsAccessories", "sending PostFlatsAccessoriesTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<PostFlatsAccessoriesRequest> request = new HttpEntity<>(objToPass, headers);
        String url = Config.getHost() + Config.getFlatAccessoriesEndpoint(flatId);

        ResponseEntity<PostFlatsAccessoriesResponse> postFlatsAccessoriesResponseResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                request,
                PostFlatsAccessoriesResponse.class);
        return postFlatsAccessoriesResponseResponseEntity;
    }


    public static PostFlatsAccessoriesResponse unpack(AsyncTask<PostFlatsAccessoriesTask.ParamStruct, Void, ResponseEntity<PostFlatsAccessoriesResponse>>   asyncTask)
    {
        try{
            ResponseEntity<PostFlatsAccessoriesResponse> responseEntity = asyncTask.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                PostFlatsAccessoriesResponse flatsAccessoriesResponse  = responseEntity.getBody();
                if(flatsAccessoriesResponse != null) {
                    return flatsAccessoriesResponse;
                }
                else {
                    Log.v("PostFlatsAccessoriesR", "flatsAccessoriesResponse == null");
                    return null;
                }
            }
            else{
                Log.v("PostFlatsAccessoriesR", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("PostFlatsAccessoriesR", e.toString());
        }
        return null;
    }

    public static class ParamStruct{
        PostFlatsAccessoriesRequest postFlatsAccessoriesRequest;
        Integer flatId;

        public ParamStruct(){
            flatId = new Integer(0);
            postFlatsAccessoriesRequest = new PostFlatsAccessoriesRequest();

        }
        public void setFlatId(Integer flatId) {
            this.flatId = flatId;
        }

        public void setPostFlatsAccessoriesRequest(PostFlatsAccessoriesRequest postFlatsAccessoriesRequest) {
            this.postFlatsAccessoriesRequest = postFlatsAccessoriesRequest;
        }

        public Integer getFlatId() {
            return flatId;
        }

        public PostFlatsAccessoriesRequest getPostFlatsAccessoriesRequest() {
            return postFlatsAccessoriesRequest;
        }
    }
}
