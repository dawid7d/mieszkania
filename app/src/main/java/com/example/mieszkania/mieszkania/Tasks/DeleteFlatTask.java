package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Requests.DeleteFlatRequest;
import com.example.mieszkania.mieszkania.Responses.DeleteFlatResponse;
import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;


public class DeleteFlatTask extends AsyncTask<Integer, Void, ResponseEntity<DeleteFlatResponse>> {

    @Override
    protected ResponseEntity<DeleteFlatResponse> doInBackground(Integer... integers) {
        Integer flat_id = integers[0];
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("DeleteFlatTask", "sending DeleteFlatTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        DeleteFlatRequest deleteFlatRequest = new DeleteFlatRequest(flat_id);
        HttpEntity<DeleteFlatRequest> request = new HttpEntity<>(deleteFlatRequest, headers);
        String url = Config.getHost() + Config.getFlatsEndpoint(); /* TODO fill with proper url */
        ResponseEntity<DeleteFlatResponse> getFavouritesResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.DELETE,
                request,
                DeleteFlatResponse.class);
        return getFavouritesResponseEntity;
    }

    public static DeleteFlatResponse unpack(AsyncTask<Integer, Void, ResponseEntity<DeleteFlatResponse>> responseEntityAsyncTask)
    {
        try{
            //TODO check if work when proper flat_id added
            ResponseEntity<DeleteFlatResponse> responseEntity = responseEntityAsyncTask.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                DeleteFlatResponse favouritesResponse  = responseEntity.getBody();
                if(favouritesResponse != null) {
                    return favouritesResponse;
                }
                else {
                    Log.v("DeleteFlatResponse", "DeleteFlatResponse == null");
                    return null;
                }
            }
            else{
                Log.v("DeleteFlatResponse", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("DeleteFlatResponse", e.toString());
        }
        return null;
    }

}
