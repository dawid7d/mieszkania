package com.example.mieszkania.mieszkania.Requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterRequest {

    public class User {
        private String email;
        private String password;
        private String password_confirmation;


        public String getEmail() {
            return email;
        }

        public String getPassword(){
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setPassword_confirmation(String password_confirmation) {
            this.password_confirmation = password_confirmation;
        }

        public String getPassword_confirmation() {
            return password_confirmation;
        }

        @Override
        public String toString(){
            return "User{"+
                    "email=" + this.email + '\'' +
                    ", password=" + this.password +
                    ", password_confirmation=" + this.password_confirmation +
                    '}';
        }
    }

    User user;


    public RegisterRequest() {
        user = new User();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public String toString(){
        return "RegisterRequest{"+
                "user=" + this.user +
                '}';
    }
}
