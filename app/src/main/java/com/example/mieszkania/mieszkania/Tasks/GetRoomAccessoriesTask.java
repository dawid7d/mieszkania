package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.GetRoomAccessoriesResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetRoomAccessoriesTask extends AsyncTask<Integer, Integer, ResponseEntity<GetRoomAccessoriesResponse>> {

    @Override
    protected ResponseEntity<GetRoomAccessoriesResponse> doInBackground(Integer ... params) {
        Integer flat_id = params[0];
        Integer room_id = params[1];
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetRoomAccessoriesTask", "sending GetRoomAccessoriesTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getFlatsEndpoint()+"/"+String.valueOf(flat_id)+"/"+"rooms"+"/"+String.valueOf(room_id)+"/"+"described_accessories";
        ResponseEntity<GetRoomAccessoriesResponse> RoomAccessoriesResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetRoomAccessoriesResponse.class);
        return RoomAccessoriesResponseEntity;
    }

    public static GetRoomAccessoriesResponse unpack(AsyncTask<Integer, Integer, ResponseEntity<GetRoomAccessoriesResponse>> accessoryResponseResponseEntity)
    {
        try{
            ResponseEntity<GetRoomAccessoriesResponse> responseEntity = accessoryResponseResponseEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                GetRoomAccessoriesResponse accessoryResponse = responseEntity.getBody();
                if(accessoryResponse != null) {
                    return accessoryResponse;
                }
                else {
                    Log.v("GetRoomAccessoriesTask", "GetRoomTypeAccessoriesResponese == null");
                    return null;
                }
            }
            else{
                Log.v("GetRoomAccessoriesTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetRoomAccessoriesTask", e.toString());
        }
        return null;
    }
}

