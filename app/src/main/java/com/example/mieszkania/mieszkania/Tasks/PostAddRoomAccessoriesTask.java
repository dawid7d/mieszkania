package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Requests.PostAddRoomAccessoriesRequest;
import com.example.mieszkania.mieszkania.Requests.PostFlatsAccessoriesRequest;
import com.example.mieszkania.mieszkania.Responses.PostAddRoomAccessoriesResponse;
import com.example.mieszkania.mieszkania.Responses.PostAddRoomResponse;
import com.example.mieszkania.mieszkania.Responses.PostFlatsAccessoriesResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class PostAddRoomAccessoriesTask extends AsyncTask<PostAddRoomAccessoriesTask.ParamStruct, Void, ResponseEntity<PostAddRoomAccessoriesResponse>> {

    @Override
    protected ResponseEntity<PostAddRoomAccessoriesResponse> doInBackground(PostAddRoomAccessoriesTask.ParamStruct... params) {
        PostAddRoomAccessoriesTask.ParamStruct input = params[0];
        PostAddRoomAccessoriesRequest objToPass = input.getRequest();
        Integer roomId = input.getRoomId();
        Integer flatId = input.getFlatId();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("PostFlatsAccessories", "sending PostFlatsAccessoriesTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<PostAddRoomAccessoriesRequest> request = new HttpEntity<>(objToPass, headers);
        String url = Config.getHost() + Config.getFlatsRoomsAccessoriesEndpoint(flatId, roomId);

        ResponseEntity<PostAddRoomAccessoriesResponse> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                request,
                PostAddRoomAccessoriesResponse.class);
        return responseEntity;
    }

    public static PostAddRoomAccessoriesResponse unpack(
            AsyncTask<PostAddRoomAccessoriesTask.ParamStruct, Void, ResponseEntity<PostAddRoomAccessoriesResponse>>   asyncTask)
    {
        try{
            ResponseEntity<PostAddRoomAccessoriesResponse> responseEntity = asyncTask.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                PostAddRoomAccessoriesResponse response  = responseEntity.getBody();
                if(response != null) {
                    return response;
                }
                else {
                    Log.v("PostAddRoomAccResp", "PostAddRoomAccessoriesResponse == null");
                    return null;
                }
            }
            else{
                Log.v("PostAddRoomAccResp", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("PostAddRoomAccResp", e.toString());
        }
        return null;
    }


    public static class ParamStruct {
        PostAddRoomAccessoriesRequest request;
        Integer roomId;
        Integer flatId;
        public ParamStruct(){
            request = new PostAddRoomAccessoriesRequest();
            roomId = new Integer(0);
            flatId = new Integer(0);
        }

        public Integer getRoomId() {
            return roomId;
        }

        public void setRoomId(Integer roomId) {
            this.roomId = roomId;
        }

        public PostAddRoomAccessoriesRequest getRequest() {
            return request;
        }

        public Integer getFlatId() {
            return flatId;
        }

        public void setFlatId(Integer flatId) {
            this.flatId = flatId;
        }

        public void setRequest(PostAddRoomAccessoriesRequest request) {
            this.request = request;
        }
    }
}
