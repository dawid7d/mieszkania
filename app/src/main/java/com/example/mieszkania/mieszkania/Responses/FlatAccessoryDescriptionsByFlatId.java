package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlatAccessoryDescriptionsByFlatId {
    private boolean success;
    private Data data;

    @JsonIgnoreProperties(ignoreUnknown = true)
    class Data{
        DescribedAccessory[] described_accessories;

        @JsonIgnoreProperties(ignoreUnknown = true)
        class DescribedAccessory{
            int id;
            int accessory_id;
            int accessory_description_id;
            String description;
            DescribedAccessory(){
                description = new String();
                id = 0;
                accessory_description_id = 0;
                accessory_id = 0;
            }

            public int getId() {
                return id;
            }

            public int getAccessory_description_id() {
                return accessory_description_id;
            }

            public int getAccessory_id() {
                return accessory_id;
            }

            public String getDescription() {
                return description;
            }
        }

        public DescribedAccessory[] getDescribed_accessories() {
            return described_accessories;
        }
    }

    public Data getData() {
        return data;
    }

    public boolean isSuccess() {
        return success;
    }
}
