package com.example.mieszkania.mieszkania.User;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.R;
import com.example.mieszkania.mieszkania.Requests.GetAccessoriesRequest;
import com.example.mieszkania.mieszkania.Requests.PostFlatsAccessoriesRequest;
import com.example.mieszkania.mieszkania.Responses.AccessoryDescyriptionByIdResponse;
import com.example.mieszkania.mieszkania.Responses.AccessoryResponse;
import com.example.mieszkania.mieszkania.Responses.PostFlatsAccessoriesResponse;
import com.example.mieszkania.mieszkania.Tasks.AccessoryDescriptionByIdTask;
import com.example.mieszkania.mieszkania.Tasks.GetAccessoriesTask;
import com.example.mieszkania.mieszkania.Tasks.PostFlatsAccessoriesTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserAddFlatAccessories extends AppCompatActivity {

    ArrayList<String> Accessories;
    ArrayList<Integer> Accessory_id;

    Map<Integer,List<String>> Accessories_Description;
    Map<Integer,List<Integer>> Accessories_Desc_Id;
    ListView listView;

    Map<Integer,Integer> accessory_description = new HashMap<>(); //accessory_id , accessory_description_id
    Map<Integer,String> user_description = new HashMap<>(); //accessory_id , "description"[String, default = ""]
    Map<Integer, Boolean> checkbox_map = new HashMap<>();
    Integer flat_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_add_flat_accessories);

        Bundle extras = getIntent().getExtras();
        flat_id = Integer.parseInt(extras.getString("flat_id"));
        Log.v("ave flat id = ", String.valueOf(flat_id));

        //TODO: get accessory by room_type (create AccessoriesRequest)

        Accessories = new ArrayList<>();
        Accessory_id = new ArrayList<>();
        AccessoryResponse accessoryResponse = GetAccessoriesTask.unpack(new GetAccessoriesTask().execute(GetAccessoriesRequest.FLAT));
        for (AccessoryResponse.Data.Accessory a : accessoryResponse.getData().getAccessories()){
            Log.v("Accessories",String.valueOf(a.getId())+" "+a.getName()+" "+a.getRoom_type());
            Accessories.add(a.getName());
            Accessory_id.add(a.getId());
            accessory_description.put(a.getId(),1);
            user_description.put(a.getId(),"");
            checkbox_map.put(a.getId(),Boolean.FALSE);
        }

        Accessories_Description = new HashMap<>();
        Accessories_Desc_Id = new HashMap<>();
        for (AccessoryResponse.Data.Accessory a : accessoryResponse.getData().getAccessories()){
            AccessoryDescyriptionByIdResponse accessoryDescyription =
                    AccessoryDescriptionByIdTask.unpack(new AccessoryDescriptionByIdTask().execute(new Integer(a.getId())));

            List<String> temp_name = new ArrayList();
            List<Integer> temp_id = new ArrayList();
            for (AccessoryDescyriptionByIdResponse.Data.Accessory_Descriptions aa : accessoryDescyription.getData().getAccessory_descriptions()){
                Log.v("AccessoriesDesc",aa.getId() +" "+ aa.getName());
                accessory_description.put(a.getId(),aa.getId());
                temp_name.add(aa.getName());
                temp_id.add(aa.getId());
            }
            Accessories_Description.put(a.getId(),temp_name);
            Accessories_Desc_Id.put(a.getId(),temp_id);
        }

        CustomAdapter customAdapter = new UserAddFlatAccessories.CustomAdapter();
        listView = findViewById(R.id.flat_accessories_listview);
        listView.setAdapter(customAdapter);

        Button next = findViewById(R.id.flat_accessoeires_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (Map.Entry<Integer, Integer> entry : accessory_description.entrySet()) {
                    Log.v("map acc_dec","acc_id="+entry.getKey() + ": acc_desc_id=" + entry.getValue().toString());
                }

                for (Map.Entry<Integer, String> entry : user_description.entrySet()) {
                    Log.v("map user_dec","acc_id="+entry.getKey() + ": user_desc=" + entry.getValue().toString());
                }

                for (Map.Entry<Integer, Boolean> entry : checkbox_map.entrySet()) {
                    Log.v("map user_dec","acc_id="+entry.getKey() + ": is_checked=" + entry.getValue().toString());
                }

                ArrayList<PostFlatsAccessoriesRequest> arrayList = new ArrayList<>();

                for (Map.Entry<Integer, Integer> entry : accessory_description.entrySet()) {
                    PostFlatsAccessoriesRequest request = new PostFlatsAccessoriesRequest();
                    request.getDescribed_accessory().setAccessory_id(entry.getKey());
                    request.getDescribed_accessory().setAccessory_description_id(entry.getValue());
                    request.getDescribed_accessory().setDescription(user_description.get(entry.getKey()));
                    arrayList.add(request);
                }

                for (PostFlatsAccessoriesRequest req : arrayList) {
                    PostFlatsAccessoriesTask.ParamStruct paramStruct = new PostFlatsAccessoriesTask.ParamStruct();
                    paramStruct.setPostFlatsAccessoriesRequest(req);
                    paramStruct.setFlatId(flat_id);
                    PostFlatsAccessoriesResponse response = PostFlatsAccessoriesTask.unpack(new PostFlatsAccessoriesTask().execute(paramStruct));
                    Log.v("PostFlatsAccResp", "success = " + response.isSuccess());
                }

                Intent intent = new Intent(UserAddFlatAccessories.this, UserAddRooms.class);
                Bundle extras = getIntent().getExtras();
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
    }

    class CustomAdapter extends BaseAdapter {

        Map<Integer, Integer> spinner_map = new HashMap<>();

        @Override
        public int getCount() {
            return Accessories.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.attribute_layout,null);

            final Context context = viewGroup.getContext();

            TextView accessory_name = view.findViewById(R.id.accessory_name);
            accessory_name.setText(Accessories.get(i));

            final Spinner accessory_desc_spinner = view.findViewById(R.id.accessory_descriptions);
            final ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                    android.R.layout.simple_spinner_dropdown_item,Accessories_Description.get(Accessory_id.get(i)));
            accessory_desc_spinner.setAdapter(adapter);

            if(spinner_map.containsKey(i)) {
                accessory_desc_spinner.setSelection(spinner_map.get(i));
            }

            accessory_desc_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    spinner_map.put(i,position);
                    accessory_description.put(Accessory_id.get(i), Accessories_Desc_Id.get(Accessory_id.get(i)).get(position));
                    Log.v("ave","accesory id="+Accessory_id.get(i)+" position="+position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                }
            });

            CheckBox checkBox = view.findViewById(R.id.checkBox);
            if(checkbox_map.containsKey(i)){
                checkBox.setChecked(checkbox_map.get(i));
            }
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    checkbox_map.put(i+1,Boolean.TRUE);
                }
            });

            Button add_description = view.findViewById(R.id.user_description);
            add_description.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    View view1 = getLayoutInflater().inflate(R.layout.add_accessory_description_dialog,null);
                    builder.setView(view1);
                    final AlertDialog dialog = builder.create();
                    dialog.show();

                    final EditText user_description_added = view1.findViewById(R.id.user_description);
                    user_description_added.getText();

                    Button ok = view1.findViewById(R.id.ok);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.v("ave","button id="+i);
                            Log.v("ave","user description="+user_description_added.getText());
                            user_description.put(Accessory_id.get(i), String.valueOf(user_description_added.getText()));
                            dialog.dismiss();
                        }
                    });
                }
            });

            return view;
        }
    }
}
