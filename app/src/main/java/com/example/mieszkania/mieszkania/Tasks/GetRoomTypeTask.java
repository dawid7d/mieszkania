package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.GetRoomTypes;
import com.example.mieszkania.mieszkania.Responses.GetWindowsTypesResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetRoomTypeTask extends AsyncTask<Void, Void, ResponseEntity<GetRoomTypes>> {

    @Override
    protected ResponseEntity<GetRoomTypes> doInBackground(Void ... params) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetRoomTypeTask", "sending GetRoomTypeTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getRoomTypes();
        ResponseEntity<GetRoomTypes> getRoomTypesResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetRoomTypes.class);
        return getRoomTypesResponseEntity;
    }

    public static GetRoomTypes unpack(AsyncTask<Void, Void, ResponseEntity<GetRoomTypes>>  responseEntityAsyncTask)
    {
        try{
            ResponseEntity<GetRoomTypes> responseEntity = responseEntityAsyncTask.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                GetRoomTypes roomTypes  = responseEntity.getBody();
                if(roomTypes != null) {
                    return roomTypes;
                }
                else {
                    Log.v("GetWindowsTypesResponse", "roomTypes == null");
                    return null;
                }
            }
            else{
                Log.v("GetWindowsTypesResponse", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetWindowsTypesResponse", e.toString());
        }
        return null;
    }
}
