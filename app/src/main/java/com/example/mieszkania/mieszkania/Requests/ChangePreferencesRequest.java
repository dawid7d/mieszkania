package com.example.mieszkania.mieszkania.Requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangePreferencesRequest {
    public ChangePreferencesRequest(){
        customer = new Customer();
    }
    public static class Customer {
        public Customer(){
            place = new String();
            distance = new Double(0.0);
            city_travel_time = new Double(0.0);
            car_travel_time = new Double(0.0);
        }
        String place;
        Double distance;
        Double city_travel_time;
        Double car_travel_time;

        public Double getCar_travel_time() {
            return car_travel_time;
        }

        public Double getCity_travel_time() {
            return city_travel_time;
        }

        public Double getDistance() {
            return distance;
        }

        public String getPlace() {
            return place;
        }

        public void setCar_travel_time(Double car_travel_time) {
            this.car_travel_time = car_travel_time;
        }

        public void setCity_travel_time(Double city_travel_time) {
            this.city_travel_time = city_travel_time;
        }

        public void setDistance(Double distance) {
            this.distance = distance;
        }

        public void setPlace(String place) {
            this.place = place;
        }
    }

    Customer customer;

    public Customer getCustomer() {
        return customer;
    }
}