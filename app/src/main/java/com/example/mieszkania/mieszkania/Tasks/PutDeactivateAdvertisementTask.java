package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Requests.PutDeactivateAdvertisementRequest;
import com.example.mieszkania.mieszkania.Responses.PutDeactivateAdvertisementResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;



public class PutDeactivateAdvertisementTask extends AsyncTask<Integer, Void, ResponseEntity<PutDeactivateAdvertisementResponse>> {

    @Override
    protected ResponseEntity<PutDeactivateAdvertisementResponse> doInBackground(Integer ... params) {
        Integer id = params[0];
        PutDeactivateAdvertisementRequest objToPass = new PutDeactivateAdvertisementRequest();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("PostFlatsTask", "sending PostFlatsTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<PutDeactivateAdvertisementRequest> request = new HttpEntity<>(objToPass, headers);
        String url = Config.getHost() + Config.getAdvertisementByIdEndpoint(id);
        ResponseEntity<PutDeactivateAdvertisementResponse> exchange = restTemplate.exchange(
                url,
                HttpMethod.PUT,
                request,
                PutDeactivateAdvertisementResponse.class);
        return exchange;
    }


    public static PutDeactivateAdvertisementResponse unpack(AsyncTask<Integer, Void, ResponseEntity<PutDeactivateAdvertisementResponse>>  entityAsyncTask)
    {
        try{
            ResponseEntity<PutDeactivateAdvertisementResponse> responseEntity = entityAsyncTask.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                PutDeactivateAdvertisementResponse response  = responseEntity.getBody();
                if(response != null) {
                    return response;
                }
                else {
                    Log.v("PutDeactivateAddResp", "PutDeactivateAdvertisementResponse == null");
                    return null;
                }
            }
            else{
                Log.v("PutDeactivateAddResp", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("PutDeactivateAddResp", e.toString());
        }
        return null;
    }
}
