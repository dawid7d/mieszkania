package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Requests.LoginRequest;
import com.example.mieszkania.mieszkania.Responses.LoginResponse;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;


public class LoginTask  extends AsyncTask<String, Void, ResponseEntity<LoginResponse>> {

    @Override
    protected ResponseEntity<LoginResponse> doInBackground(String... strings) {
        String email = strings[0];
        String password = strings[1];


        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        // temporary solution: this ip address should be changed to ip of host seen by ipconfig command
        // cannot be set to e.g. localhost since emulator runs as a seperate VM
        String url = Config.getHost() + Config.getLoginEndpoint();
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.getUser().setEmail(email);
        loginRequest.getUser().setPassword(password);

        ResponseEntity<LoginResponse> responseEntity =  restTemplate.postForEntity(url, loginRequest, LoginResponse.class);
        return responseEntity;
    }
}
