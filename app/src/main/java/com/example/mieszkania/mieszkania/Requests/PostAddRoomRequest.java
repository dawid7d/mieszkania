package com.example.mieszkania.mieszkania.Requests;


public class PostAddRoomRequest {

    public PostAddRoomRequest(){
        room = new Room();
    }

    public static class Room{
        public Room(){
            room_type = new String();
            yardage = new Double(0.0);
            total_places = new Integer(0);
            can_rent_separately = false;
        }
        String room_type;
        Double yardage;
        Integer total_places;
        boolean can_rent_separately;

        public Double getYardage() {
            return yardage;
        }

        public Integer getTotal_places() {
            return total_places;
        }

        public String getRoom_type() {
            return room_type;
        }

        public boolean isCan_rent_separately() {
            return can_rent_separately;
        }

        public void setYardage(Double yardage) {
            this.yardage = yardage;
        }

        public void setCan_rent_separately(boolean can_rent_separately) {
            this.can_rent_separately = can_rent_separately;
        }

        public void setRoom_type(String room_type) {
            this.room_type = room_type;
        }

        public void setTotal_places(Integer total_places) {
            this.total_places = total_places;
        }
    }
    Room room;

    public Room getRoom() {
        return room;
    }
}
