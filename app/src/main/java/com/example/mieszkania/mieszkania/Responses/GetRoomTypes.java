package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class GetRoomTypes {
    Boolean success;
    Data data;

    public static class Data {
        String[] room_types;

        public String[] getRoom_types() {
            return room_types;
        }
    }

    public Boolean getSuccess() {
        return success;
    }

    public Data getData() {
        return data;
    }
}
