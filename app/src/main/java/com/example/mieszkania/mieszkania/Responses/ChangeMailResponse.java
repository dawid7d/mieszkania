package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangeMailResponse {
    boolean success;

    public ChangeMailResponse(){
    }

    public class Data {
        public class User {
            int id;
            String email;

            public int getId() {
                return id;
            }

            public String getEmail() {
                return email;
            }
        }
        User user;
    }
    Data data;

    public boolean isSuccess() {
        return success;
    }


}
