package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.GetUserReservationsResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetUserReservationsTask extends AsyncTask<Void, Void, ResponseEntity<GetUserReservationsResponse>> {

    @Override
    protected ResponseEntity<GetUserReservationsResponse> doInBackground(Void ... params) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetUserReservationsTask", "sending GetUserReservationsTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getReservations();
        ResponseEntity<GetUserReservationsResponse> GetUserReservationsEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetUserReservationsResponse.class);
        return GetUserReservationsEntity;
    }

    public static GetUserReservationsResponse unpack(AsyncTask<Void, Void, ResponseEntity<GetUserReservationsResponse>>  responseEntityAsyncTask)
    {
        try{
            ResponseEntity<GetUserReservationsResponse> responseEntity = responseEntityAsyncTask.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                GetUserReservationsResponse roomTypes  = responseEntity.getBody();
                if(roomTypes != null) {
                    return roomTypes;
                }
                else {
                    Log.v("GetWindowsTypesResponse", "roomTypes == null");
                    return null;
                }
            }
            else{
                Log.v("GetWindowsTypesResponse", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetWindowsTypesResponse", e.toString());
        }
        return null;
    }
}

