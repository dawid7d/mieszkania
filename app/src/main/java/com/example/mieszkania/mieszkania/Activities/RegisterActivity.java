package com.example.mieszkania.mieszkania.Activities;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mieszkania.mieszkania.R;
import com.example.mieszkania.mieszkania.Responses.RegisterResponse;
import com.example.mieszkania.mieszkania.Tasks.RegisterTask;

public class RegisterActivity extends AppCompatActivity {

    Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        register = findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = ((EditText) findViewById(R.id.name)).getText().toString();
                String surname = ((EditText) findViewById(R.id.surname)).getText().toString();
                String email = ((EditText) findViewById(R.id.phone)).getText().toString();
                String phone = ((EditText) findViewById(R.id.email)).getText().toString();
                String password = ((EditText) findViewById(R.id.password)).getText().toString();
                String con_password = ((EditText) findViewById(R.id.conf_password)).getText().toString();
                //TODO: add password confirmation instead of passing password two times
                //TODO: validation password vs password_confirmation

                AsyncTask<String, Void, RegisterResponse> registerResponseAsyncTask = new RegisterTask().execute(email, password, con_password);
                try
                {
                    RegisterResponse registerResponse = registerResponseAsyncTask.get();
                    Log.v("registerResponse", registerResponse.toString());
                }
                catch (Exception e)
                {
                    Log.v("register:", e.toString());
                }

            }
        });

    }
}
