package com.example.mieszkania.mieszkania;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.Responses.AdvertisementsResponse;
import com.example.mieszkania.mieszkania.Responses.FlatByIdResponse;
import com.example.mieszkania.mieszkania.Responses.FlatsResponse;
import com.example.mieszkania.mieszkania.Responses.GetRoomByIdResponse;
import com.example.mieszkania.mieszkania.Tasks.AdvertisementsTask;
import com.example.mieszkania.mieszkania.Tasks.GetFlatByIdTask;
import com.example.mieszkania.mieszkania.Tasks.GetFlatsListTask;
import com.example.mieszkania.mieszkania.Tasks.GetRoomByIdTask;
import com.example.mieszkania.mieszkania.User.UserState;

import java.util.ArrayList;

public class GuestAdvertisement extends AppCompatActivity {

    ArrayList<FlatByIdResponse.Data.Flat> flats_list;
    static ArrayList<AdvertisementsResponse.Data.Advertisements> advertisements_list;
    Boolean is_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_advertisement);

        flats_list = new ArrayList<>();
        advertisements_list = new ArrayList<>();

        // TODO: auto
        AdvertisementsResponse advertisementsResponse = AdvertisementsTask.unpack(new AdvertisementsTask().execute());
        for(AdvertisementsResponse.Data.Advertisements a : advertisementsResponse.getData().getAdvertisement()){
            Log.v("ave","id=" + a.getId()+
                    " user id=" + a.getUser_id()+
                    " target id= " + a.getTarget_id()+
                    " target type= " + a.getTarget_type()+
                    " rental_price" + a.getRental_price()+
                    " start_data" + a.getStart_rent_data()+
                    " end_data" + a.getEnd_rent_data()+
                    " contract type" + a.getContract_type());
            advertisements_list.add(a);
        }

//        FlatsResponse flatsResponse = GetFlatsListTask.unpack(new GetFlatsListTask().execute());
//        for (FlatsResponse.Data.Flat f :
//                flatsResponse.getData().getFlats()) {
//            Log.v("FlatsResponse", String.valueOf(f.getId()));
//        }
//        for(FlatsResponse.Data.Flat f : flatsResponse.getData().getFlats()){
//            FlatByIdResponse flatByIdResponse =  GetFlatByIdTask.unpack(new GetFlatByIdTask().execute(new Integer(f.getId())));
//            flats_list.add(flatByIdResponse.getData().getFlat());
//            Log.v("FlatByIdResponse", flatByIdResponse.getData().getFlat().getAddress_city());
//        }

        //TODO: get data form DataBase
//        Integer[] img = {R.drawable.img1, R.drawable.img_2, R.drawable.img_3, R.drawable.img_4, R.drawable.img_5, R.drawable.img_6};
//        String[] descriotion = {"Mieszkanie 2 pokoje + kuchnia", "Mieszkanie dla studentów", "Mieszkanie 3 pokoje 1 osobowe", "Mieszkanie 2 pokoje + kuchnia", "Mieszkanie dla studentów", "Mieszkanie 3 pokoje 1 osobowe"};
//        String[] price = {"439 zł", "953 zł", "1090 zł", "900 zł", "867 zł", "758 zł"};
//        String[] date = {"03.04.2018 - 03.05.2018", "03.04.2018 - 03.05.2018", "03.04.2018 - 03.05.2018", "03.04.2018 - 03.05.2018", "03.04.2018 - 03.05.2018", "03.04.2018 - 03.05.2018"};
//        ArrayList<Integer> id = new ArrayList<>();
//        for(int i=1; i<=img.length; i++){id.add(i);}

        final ListView listView = findViewById(R.id.flatsListView);
        AdvertisementsAdapter advertisementsAdapter = new AdvertisementsAdapter(advertisements_list);
//        FlatListView flatListView = new FlatListView(this, img, descriotion, price, date, id);
        listView.setAdapter(advertisementsAdapter);

        is_login = false;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.v("getItemIdAtPosition(): ", String.valueOf(listView.getItemIdAtPosition(i)));
                Log.v("ave","flat id="+advertisements_list.get(i).getTarget_id());
                if(advertisements_list.get(i).getTarget_type().equals("Flat")){
                    Log.v("ave", "target id to GET="+advertisements_list.get(i).getTarget_id());
                    int selectedFlatId = advertisements_list.get(i).getTarget_id();
                    FlatByIdResponse flatByIdResponse =  GetFlatByIdTask.unpack(new GetFlatByIdTask().execute(new Integer(selectedFlatId)));

                    Intent intent = new Intent(GuestAdvertisement.this,Flat.class);
                    UserState.setCanEdit(false);
                    intent.putExtra("can_edit", UserState.getCanEdit());
                    intent.putExtra("Flat", flatByIdResponse.getData().getFlat());
                    startActivity(intent);
                } else {
                    Log.v("ave", "target id to GET="+advertisements_list.get(i).getTarget_id());
                    int selectedRoomId = advertisements_list.get(i).getTarget_id();
                    GetRoomByIdResponse RoomByIdResponse = GetRoomByIdTask.unpack(new GetRoomByIdTask().execute(new Integer(selectedRoomId)));
                    GetRoomByIdResponse.Data.Room room = RoomByIdResponse.getData().getRoom();
                    Intent intent = new Intent(GuestAdvertisement.this,Room.class);
                    UserState.setCanEdit(false);
                    intent.putExtra("can_edit", UserState.getCanEdit());
                    intent.putExtra("RoomById", RoomByIdResponse.getData().getRoom());
                    intent.putExtra("flat_id", RoomByIdResponse.getData().getRoom().getFlat_id());
                    startActivity(intent);
                }
            }
        });
    }

    public class AdvertisementsAdapter extends BaseAdapter {
        ArrayList<AdvertisementsResponse.Data.Advertisements> advertisements_list;

        AdvertisementsAdapter(ArrayList<AdvertisementsResponse.Data.Advertisements> _advertisements_list){
            this.advertisements_list = _advertisements_list;
        }

        @Override
        public int getCount() {
            return advertisements_list.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.flat_adapter_layout,null);

            TextView textView = view.findViewById(R.id.textView);
            String ad_active = "";
            if(advertisements_list.get(i).isIs_active()){ad_active="Tak";}
            else {ad_active="Nie";}
            textView.setText(" \n"+advertisements_list.get(i).getTitle()+"\n\n"+
                    " Koszt miesieczny: "+advertisements_list.get(i).getRental_price()+"zł \n"+
                    " Zaliczka: "+advertisements_list.get(i).getDeposit()+"zł\n\n"+
                    " Czy aktywne: "+ad_active+"\n\n"+
                    " Od: "+advertisements_list.get(i).getStart_rent_data()+"\n"+
                    " Od: "+advertisements_list.get(i).getEnd_rent_data()+"\n"
            );

            return view;
        }
    }
}
