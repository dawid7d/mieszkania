package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Requests.RegisterRequest;
import com.example.mieszkania.mieszkania.Responses.RegisterResponse;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;


public class RegisterTask extends AsyncTask<String, Void, RegisterResponse> {

    @Override
    protected RegisterResponse doInBackground(String... strings) {
        String email = strings[0];
        String password = strings[1];
        String password_confirmation = strings[2];

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        // temporary solution: this ip address should be changed to ip of host seen by ipconfig command
        // cannot be set to e.g. localhost since emulator runs as a seperate VM
        String url = Config.getHost() + Config.getRegisterEndpoint();
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.getUser().setEmail(email);
        registerRequest.getUser().setPassword(password);
        registerRequest.getUser().setPassword_confirmation(password_confirmation);
        ResponseEntity<RegisterResponse> responseEntity = restTemplate.postForEntity(url, registerRequest, RegisterResponse.class);
        RegisterResponse resp = responseEntity.getBody();

        return resp;
    }
}
