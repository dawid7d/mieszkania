package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.GetWindowsTypesResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetWindowsTypesTask extends AsyncTask<Void, Void, ResponseEntity<GetWindowsTypesResponse>> {

    @Override
    protected ResponseEntity<GetWindowsTypesResponse> doInBackground(Void ... params) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetWindowsTypesTask", "sending GetWindowsTypesTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getWindowsTypesEndpoint();
        ResponseEntity<GetWindowsTypesResponse> getWindowsTypesResponseResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetWindowsTypesResponse.class);
        return getWindowsTypesResponseResponseEntity;
    }

    public static GetWindowsTypesResponse unpack(AsyncTask<Void, Void, ResponseEntity<GetWindowsTypesResponse>>  responseEntityAsyncTask)
    {
        try{
            ResponseEntity<GetWindowsTypesResponse> responseEntity = responseEntityAsyncTask.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                GetWindowsTypesResponse flatsResponse  = responseEntity.getBody();
                if(flatsResponse != null) {
                    return flatsResponse;
                }
                else {
                    Log.v("GetWindowsTypesTask", "flatsResponse == null");
                    return null;
                }
            }
            else{
                Log.v("GetWindowsTypesTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetWindowsTypesTask", e.toString());
        }
        return null;
    }
}
