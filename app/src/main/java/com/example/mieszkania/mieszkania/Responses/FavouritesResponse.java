package com.example.mieszkania.mieszkania.Responses;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class FavouritesResponse {
    boolean success;

    public static class Data {

        Favourites[] favourites;

        public Favourites[] getFavourites(){
            return favourites;
        }

        public static class Favourites {

            String id;
            int user_id;
            int advertisement_id;

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public int getAdvertisement_id() {
                return advertisement_id;
            }

            public void setAdvertisement_id(int advertisement_id) {
                this.advertisement_id = advertisement_id;
            }
        }
    }

    Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }
}
