package com.example.mieszkania.mieszkania.Responses;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessoryResponse {
    boolean success;

    public static class Data {

        public static class Accessory{
            private int id;
            private String name;
            private String room_type;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getRoom_type() {
                return room_type;
            }

            public void setRoom_type(String room_type) {
                this.room_type = room_type;
            }
        }

        public void setAccessories(Accessory[] accessories) {
            this.accessories = accessories;
        }

        public Accessory[] getAccessories() {
            return accessories;
        }

        Accessory[] accessories;
    }

    Data data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
