package com.example.mieszkania.mieszkania.Requests;


public class PostAdvertisementRequest {

    public static final String fixedTerm = "fixed_term";
    public static final String indefinite = "indefinite";

    public static final String targetFlat = "Flat";
    public static final String targetRoom = "Room";

    public PostAdvertisementRequest(){
    }

    String target_type = "";
    Integer target_id = 0;
    String title = "";
    Double rental_price = 0.0;
    Double deposit = 0.0;
    String start_rent_date = "";
    String end_rent_date = "";
    String contract_type = "";
    boolean agency = false;
    Double agency_commission = 0.0;

    public Double getAgency_commission() {
        return agency_commission;
    }

    public Double getDeposit() {
        return deposit;
    }

    public Double getRental_price() {
        return rental_price;
    }

    public Integer getTarget_id() {
        return target_id;
    }

    public String getContract_type() {
        return contract_type;
    }

    public String getEnd_rent_date() {
        return end_rent_date;
    }

    public String getStart_rent_date() {
        return start_rent_date;
    }

    public String getTarget_type() {
        return target_type;
    }

    public String getTitle() {
        return title;
    }

    public void setAgency(boolean agency) {
        this.agency = agency;
    }

    public void setAgency_commission(Double agency_commission) {
        this.agency_commission = agency_commission;
    }

    public void setContract_type(String contract_type) {
        this.contract_type = contract_type;
    }

    public void setDeposit(Double deposit) {
        this.deposit = deposit;
    }

    public void setEnd_rent_date(String end_rent_date) {
        this.end_rent_date = end_rent_date;
    }

    public void setRental_price(Double rental_price) {
        this.rental_price = rental_price;
    }

    public void setStart_rent_date(String start_rent_date) {
        this.start_rent_date = start_rent_date;
    }

    public void setTarget_id(Integer target_id) {
        this.target_id = target_id;
    }

    public void setTarget_type(String target_type) {
        this.target_type = target_type;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
