package com.example.mieszkania.mieszkania;

public class Config {

//    private static String host = "http:/192.168.1.4:41234";
//    private static String host = "http:/192.168.0.213:41234";
    private static String host = "https://flatservice-api.herokuapp.com";
    private static String loginEndpoint = "/users/sign_in";
    private static String registerEndpoint = "/users/sign_up";
    private static String logoutEndpoint = "/users/sign_out";
    private static String personalDataEndpoint = "/users/personal_data";
    private static String flatsEndpoint = "/flats";
    private static String preferencesEndpoint = "/users/preferences";
    private static String changePreferencesEndpoint = "/users/change_preferences";
    private static String buildingTypesEndpoint = "/building_types";
    private static String windowsTypesEndpoint = "/windows_types";
    private static String accessoriesEndpoint= "/accessories";
    private static String roomTypes = "/room_types";
    private static String advertisements = "/advertisements";
    private static String favourites = "/favourites";
    private static String rooms = "/rooms";
    private static String reservations = "/users/reservations";

    public static void setHost(String host) {
        Config.host = host;
    }

    public static String getReservations() {
        return reservations;
    }

    public static String getRooms() {
        return rooms;
    }

    public static String getHost() {
        return host;
    }

    public static String getLoginEndpoint() {
        return loginEndpoint;
    }

    public static String getRegisterEndpoint() {
        return registerEndpoint;
    }

    public static String getLogoutEndpoint() {
        return logoutEndpoint;
    }

    public static String getPersonalDataEndpoint() {
        return personalDataEndpoint;
    }

    public static String getFlatsEndpoint() {
        return flatsEndpoint;
    }

    public static String getPreferencesEndpoint() {
        return preferencesEndpoint;
    }

    public static String getChangePreferencesEndpoint() {
        return changePreferencesEndpoint;
    }

    public static String getBuildingTypesEndpoint() {
        return buildingTypesEndpoint;
    }

    public static String getWindowsTypesEndpoint() {
        return windowsTypesEndpoint;
    }

    public static String getAccessoriesEndpoint() { return accessoriesEndpoint; }

    public static String getRoomTypes() {
        return roomTypes;
    }

    public static String getAdvertisementsEndpoint() {
        return advertisements ;
    }
    public static String getAdvertisementByIdEndpoint(Integer id) {
        return advertisements + "/" + id.toString();
    }

    public static String getFavourites() {
        return favourites;
    }

    public static String getFlatsRoomsAccessoriesEndpoint(Integer flatId, Integer roomId){
        return getFlatsEndpoint() + "/" + flatId.toString() + "/rooms/" + roomId.toString() + "/described_accessories";
    }
    public static String getFlatAccessoriesEndpoint(Integer flatId){
        return getFlatsEndpoint() + "/" + flatId.toString() + "/described_accessories";
    }
    public static String getFlatRoomsEndpoint(Integer flatId){
        return getFlatsEndpoint() + "/" + flatId.toString() + "/rooms";
    }
}