package com.example.mieszkania.mieszkania.Responses;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessoryDescyriptionByIdResponse {
    boolean success;

    public static class Data {

        public static class Accessory_Descriptions{
            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public Accessory_Descriptions[] getAccessory_descriptions() {
            return accessory_descriptions;
        }

        public void setAccessory_descriptions(Accessory_Descriptions[] accessory_descriptions) {
            this.accessory_descriptions = accessory_descriptions;
        }

        Accessory_Descriptions[] accessory_descriptions;
    }

    Data data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
