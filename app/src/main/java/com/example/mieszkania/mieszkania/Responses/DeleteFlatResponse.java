package com.example.mieszkania.mieszkania.Responses;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteFlatResponse {
    boolean success;
    String error[];

    public String[] getError() {
        return error;
    }

    public boolean isSuccess() {
        return success;
    }
}
