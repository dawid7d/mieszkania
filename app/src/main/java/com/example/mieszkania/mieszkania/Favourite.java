package com.example.mieszkania.mieszkania;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.Responses.AdvertisementsByIdResponse;
import com.example.mieszkania.mieszkania.Responses.FavouritesResponse;
import com.example.mieszkania.mieszkania.Responses.FlatByIdResponse;
import com.example.mieszkania.mieszkania.Responses.GetRoomByIdResponse;
import com.example.mieszkania.mieszkania.Tasks.AdvertisementsTaskByIdTask;
import com.example.mieszkania.mieszkania.Tasks.FavouritesTask;
import com.example.mieszkania.mieszkania.Tasks.GetFlatByIdTask;
import com.example.mieszkania.mieszkania.Tasks.GetRoomByIdTask;
import com.example.mieszkania.mieszkania.User.UserState;

import java.util.ArrayList;

public class Favourite extends AppCompatActivity {

    static ArrayList<AdvertisementsByIdResponse.Data.Advertisement> advertisements_list;
    ListView listView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);
        advertisements_list = new ArrayList<>();

        FavouritesResponse favouritesResponse = FavouritesTask.unpack(new FavouritesTask().execute());
        for (FavouritesResponse.Data.Favourites f : favouritesResponse.getData().getFavourites()){
            Log.v("ave"," f ad_id = "+f.getAdvertisement_id());
            Log.v("ave"," f user_id = "+f.getUser_id());
        }
        for (FavouritesResponse.Data.Favourites f : favouritesResponse.getData().getFavourites()){
            AdvertisementsByIdResponse advertisementsByIdResponse = AdvertisementsTaskByIdTask.unpack(new AdvertisementsTaskByIdTask().execute(new Integer(f.getAdvertisement_id())));
            advertisements_list.add(advertisementsByIdResponse.getData().getAdvertisement());
        }
        for(AdvertisementsByIdResponse.Data.Advertisement a : advertisements_list){
            Log.v("ave","id=" + a.getId()+
                    " user id=" + a.getUser_id()+
                    " target id= " + a.getTarget_id()+
                    " target type= " + a.getTarget_type()+
                    " rental_price" + a.getRental_price()+
                    " start_data" + a.getStart_rent_date()+
                    " end_data" + a.getEnd_rent_date()+
                    " contract type" + a.getContract_type());
        }

        FavouriteAdapter favouriteAdapter = new FavouriteAdapter(advertisements_list);
        listView2 = findViewById(R.id.fav_list);
        listView2.setAdapter(favouriteAdapter);
    }

    public class FavouriteAdapter extends BaseAdapter {
        ArrayList<AdvertisementsByIdResponse.Data.Advertisement> user_flats_list;

        FavouriteAdapter(ArrayList<AdvertisementsByIdResponse.Data.Advertisement> _user_flats_list){
            this.user_flats_list = _user_flats_list;
        }

        @Override
        public int getCount() {
            return user_flats_list.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.favourites_layout,null);

            TextView textView = view.findViewById(R.id.textView);
            String ad_active = "";
            if(advertisements_list.get(i).isIs_active()){ad_active="Tak";}
            else {ad_active="Nie";}
            textView.setText(" \n"+advertisements_list.get(i).getTitle()+"\n\n"+
                    " Koszt miesieczny: "+advertisements_list.get(i).getRental_price()+"zł \n"+
                    " Zaliczka: "+advertisements_list.get(i).getDeposit()+"zł\n\n"+
                    " Czy aktywne: "+ad_active+"\n\n"+
                    " Od: "+advertisements_list.get(i).getStart_rent_date()+"\n"+
                    " Od: "+advertisements_list.get(i).getEnd_rent_date()+"\n"
            );

            Button got_to_flat = view.findViewById(R.id.go_to_flat);
            got_to_flat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(advertisements_list.get(i).getTarget_type().equals("Flat")){
                        Log.v("ave", "target id to GET="+advertisements_list.get(i).getTarget_id());
                        int selectedFlatId = advertisements_list.get(i).getTarget_id();
                        FlatByIdResponse flatByIdResponse =  GetFlatByIdTask.unpack(new GetFlatByIdTask().execute(new Integer(selectedFlatId)));

                        Intent intent = new Intent(Favourite.this,Flat.class);
                        UserState.setCanEdit(false);
                        intent.putExtra("can_edit", UserState.getCanEdit());
                        intent.putExtra("Flat", flatByIdResponse.getData().getFlat());
//                        intent.putExtra("Advertisement", (CharSequence) advertisements_list.get(i)); //TODO: sending advertismens not working
                        startActivity(intent);
                    } else {
                        Log.v("ave", "target id to GET="+advertisements_list.get(i).getTarget_id());
                        int selectedRoomId = advertisements_list.get(i).getTarget_id();
                        GetRoomByIdResponse RoomByIdResponse = GetRoomByIdTask.unpack(new GetRoomByIdTask().execute(new Integer(selectedRoomId)));
                        GetRoomByIdResponse.Data.Room room = RoomByIdResponse.getData().getRoom();
                        Intent intent = new Intent(Favourite.this,Room.class);
                        UserState.setCanEdit(false);
                        intent.putExtra("can_edit", UserState.getCanEdit());
                        intent.putExtra("RoomById", RoomByIdResponse.getData().getRoom());
                        intent.putExtra("flat_id", RoomByIdResponse.getData().getRoom().getFlat_id());
                        startActivity(intent);
                    }
                }
            });

            Button delete_favourite = view.findViewById(R.id.delete);
            delete_favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //TODO: delete favourite
                    Log.v("##","dupa1");
                }
            });

            return view;
        }
    }
}
