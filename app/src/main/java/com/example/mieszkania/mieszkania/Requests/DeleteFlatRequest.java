package com.example.mieszkania.mieszkania.Requests;

public class DeleteFlatRequest {
    public static class Flat{
        Integer id;
        public Flat(){
            id = new Integer(0);
        }
        public Flat(Integer flat_id){
            id = flat_id;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }
    Flat[] flats;
    public DeleteFlatRequest(Integer flat_id){
        flats = new Flat[1];
        flats[0] = new Flat(flat_id);
    }
}
