package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Requests.ChangePreferencesRequest;
import com.example.mieszkania.mieszkania.Responses.ChangePreferencesResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class ChangePreferencesTask extends AsyncTask<ChangePreferencesRequest, Void, ResponseEntity<ChangePreferencesResponse>> {

    @Override
    protected ResponseEntity<ChangePreferencesResponse> doInBackground(ChangePreferencesRequest... params) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetPreferencesTask", "sending GetPreferencesReq with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpEntity<ChangePreferencesRequest> request = new HttpEntity<>(params[0], headers);

        String url = Config.getHost() + Config.getChangePreferencesEndpoint();
        return restTemplate.exchange(
                url,
                HttpMethod.PUT,
                request,
                ChangePreferencesResponse.class);
    }

    public static ChangePreferencesResponse unpack(AsyncTask<ChangePreferencesRequest, Void, ResponseEntity<ChangePreferencesResponse>>  changePreferencesRequestVoidResponseEntityAsyncTask)
    {
        try{
            ResponseEntity<ChangePreferencesResponse> responseEntity = changePreferencesRequestVoidResponseEntityAsyncTask.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                ChangePreferencesResponse changePreferencesResponse  = responseEntity.getBody();
                if(changePreferencesResponse != null) {
                    return changePreferencesResponse;
                }
                else {
                    Log.v("ChangePreferencesTask", "ChangePreferencesResponse == null");
                    return null;
                }
            }
            else{
                Log.v("ChangePreferencesTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("ChangePreferencesTask", e.toString());
        }
        return null;
    }
}

