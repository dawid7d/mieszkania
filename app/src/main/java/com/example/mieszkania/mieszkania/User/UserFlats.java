package com.example.mieszkania.mieszkania.User;

 import android.content.Intent;
 import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
 import android.util.Log;
 import android.view.View;
 import android.widget.AdapterView;
 import android.widget.Button;
 import android.widget.ListView;

 import com.example.mieszkania.mieszkania.Activities.AplicationActivity;
 import com.example.mieszkania.mieszkania.Flat;
 import com.example.mieszkania.mieszkania.FlatListView;
 import com.example.mieszkania.mieszkania.R;

 import java.util.ArrayList;

public class UserFlats extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_flats);

        Button user_flats = findViewById(R.id.user_flats);
        user_flats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserFlats.this,UsersOwnFlats.class);
                startActivity(intent);
            }
        });

        Button add_flat = findViewById(R.id.add_new_flat);
        add_flat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserFlats.this,UserAddNewFlat.class);
                startActivity(intent);
            }
        });

        Button user_active_advertisements = findViewById(R.id.user_active_advertisements);
        user_active_advertisements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserFlats.this,UserActiveAdvertisements.class);
                startActivity(intent);
            }
        });

        Button return_to_main_page = findViewById(R.id.return_to_main);
        return_to_main_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserFlats.this,AplicationActivity.class);
                startActivity(intent);
            }
        });

    }
}
