package com.example.mieszkania.mieszkania.User;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.Flat;
import com.example.mieszkania.mieszkania.FlatListView;
import com.example.mieszkania.mieszkania.R;
import com.example.mieszkania.mieszkania.Responses.AdvertisementsResponse;
import com.example.mieszkania.mieszkania.Responses.FlatByIdResponse;
import com.example.mieszkania.mieszkania.Responses.FlatsResponse;
import com.example.mieszkania.mieszkania.Responses.GetFlatAccessoriesResponse;
import com.example.mieszkania.mieszkania.Tasks.AdvertisementsTask;
import com.example.mieszkania.mieszkania.Tasks.GetFlatAccessoriesTask;
import com.example.mieszkania.mieszkania.Tasks.GetFlatByIdTask;
import com.example.mieszkania.mieszkania.Tasks.GetFlatsListTask;

import java.util.ArrayList;

public class UsersOwnFlats extends AppCompatActivity {

    ArrayList<Integer> id;
    ArrayList<FlatByIdResponse.Data.Flat> user_flats_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_own_flats);

        id = new ArrayList<>();
        user_flats_list = new ArrayList<>();

        FlatsResponse flatsResponse = GetFlatsListTask.unpack(new GetFlatsListTask().execute());
        for (FlatsResponse.Data.Flat f : flatsResponse.getData().getFlats()) {
            Log.v("FlatsResponse", String.valueOf(f.getId()));
        }
        for(FlatsResponse.Data.Flat f : flatsResponse.getData().getFlats()){
            FlatByIdResponse flatByIdResponse =  GetFlatByIdTask.unpack(new GetFlatByIdTask().execute(new Integer(f.getId())));
            user_flats_list.add(flatByIdResponse.getData().getFlat());
            Log.v("FlatByIdResponse", flatByIdResponse.getData().getFlat().getAddress_city());
        }

        for(FlatByIdResponse.Data.Flat flat : user_flats_list){
            id.add(flat.getId());
        }


        final ListView listView2 = findViewById(R.id.flatsListView);
        FlatAdapter flatAdapter = new FlatAdapter(user_flats_list);
        listView2.setAdapter(flatAdapter);
        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.v("getItemIdAtPosition(): ", String.valueOf(listView2.getItemIdAtPosition(i)));
                int selectedFlatId = user_flats_list.get(i).getId();
                FlatByIdResponse flatByIdResponse =  GetFlatByIdTask.unpack(new GetFlatByIdTask().execute(new Integer(selectedFlatId)));

                Intent intent = new Intent(UsersOwnFlats.this,Flat.class);
                intent.putExtra("is_login", UserState.getIsLogged());
                UserState.setCanEdit(true);
                intent.putExtra("can_edit", UserState.getCanEdit());
                intent.putExtra("Flat", flatByIdResponse.getData().getFlat());
                startActivity(intent);
            }
        });
    }
    public class FlatAdapter extends BaseAdapter {
        ArrayList<FlatByIdResponse.Data.Flat> user_flats_list;

        FlatAdapter(ArrayList<FlatByIdResponse.Data.Flat> _user_flats_list){
            this.user_flats_list = _user_flats_list;
        }

        @Override
        public int getCount() {
            return user_flats_list.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.flat_adapter_layout,null);

            TextView textView = view.findViewById(R.id.textView);
            textView.setText("\n"+
                    " Miasto: "+ user_flats_list.get(i).getAddress_city()+"\n"+
                    " Ulica: "+ user_flats_list.get(i).getAddress_street()+"\n"+
                    " Nr. budynku: " + user_flats_list.get(i).getAddress_building_number()+"\n"+
                    " Mieszkanie: " + user_flats_list.get(i).getAddress_flat_number()+"\n"+
                    " Piętro: " + user_flats_list.get(i).getFloor()+"\n"+
                    " Typ budynku: " + user_flats_list.get(i).getBuilding_type()+"\n"+
                    " Liczba pomieszczen: " + user_flats_list.get(i).getRooms_number()+"\n"+
                    " Powierzchnia: " + user_flats_list.get(i).getYardage()+" m^2\n"+
                    " Koszt za media:" + user_flats_list.get(i).getMedia_cost()+" zł\n"
//                    "id=" + user_flats_list.get(i).getId()
            );

            return view;
        }
    }
}
