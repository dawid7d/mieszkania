package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FlatByIdResponse {
    boolean success;


    public static class Data {
        Flat flat;

        public Flat getFlat() {
            return flat;
        }

        public void setFlat(Flat flat) {
            this.flat = flat;
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Flat implements Serializable{
            int id;
            double additional_rent_cost;
            String address_building_number;
            String address_city;
            String address_flat_number;
            String address_street;
            String building_type;
            int building_year;
            int floor;
            double media_cost;
            int rooms_number;
            String windows_type;
            double yardage;
            String address_gps;
            String created_at;
            String updated_at;
            String user_id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public double getAdditional_rent_cost() {
                return additional_rent_cost;
            }

            public void setAdditional_rent_cost(double additional_rent_cost) {
                this.additional_rent_cost = additional_rent_cost;
            }

            public String getAddress_building_number() {
                return address_building_number;
            }

            public double getMedia_cost() {
                return media_cost;
            }

            public String getAddress_city() {
                return address_city;
            }

            public String getAddress_flat_number() {
                return address_flat_number;
            }

            public String getAddress_street() {
                return address_street;
            }

            public int getBuilding_year() {
                return building_year;
            }

            public int getFloor() {
                return floor;
            }

            public String getBuilding_type() {
                return building_type;
            }

            public int getRooms_number() {
                return rooms_number;
            }

            public double getYardage() {
                return yardage;
            }

            public String getWindows_type() {
                return windows_type;
            }

            public void setYardage(double yardage) {
                this.yardage = yardage;
            }

            public void setAddress_building_number(String address_building_number) {
                this.address_building_number = address_building_number;
            }

            public void setAddress_city(String address_city) {
                this.address_city = address_city;
            }

            public void setAddress_flat_number(String address_flat_number) {
                this.address_flat_number = address_flat_number;
            }

            public void setAddress_street(String address_street) {
                this.address_street = address_street;
            }

            public void setBuilding_type(String building_type) {
                this.building_type = building_type;
            }

            public void setBuilding_year(int building_year) {
                this.building_year = building_year;
            }

            public void setFloor(int floor) {
                this.floor = floor;
            }

            public void setMedia_cost(double media_cost) {
                this.media_cost = media_cost;
            }

            public void setRooms_number(int rooms_number) {
                this.rooms_number = rooms_number;
            }

            public void setWindows_type(String windows_type) {
                this.windows_type = windows_type;
            }
        }

    }
    Data data;

    public boolean isSuccess() {
        return success;
    }

    public Data getData() {
        return data;
    }
}
