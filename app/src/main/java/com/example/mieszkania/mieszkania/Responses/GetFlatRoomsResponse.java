package com.example.mieszkania.mieszkania.Responses;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFlatRoomsResponse {
    boolean success;

    Data data;

    public static class Data {

        Rooms[] rooms;

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Rooms implements Serializable {
            private int id;
            private int flat_id;
            private int yardage;
            private int total_places;
            private int available_places;
            private String room_type;
            private Boolean can_rent_separately;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getFlat_id() {
                return flat_id;
            }

            public void setFlat_id(int flat_id) {
                this.flat_id = flat_id;
            }

            public int getYardage() {
                return yardage;
            }

            public void setYardage(int yardage) {
                this.yardage = yardage;
            }

            public int getTotal_places() {
                return total_places;
            }

            public void setTotal_places(int total_places) {
                this.total_places = total_places;
            }

            public int getAvailable_places() {
                return available_places;
            }

            public void setAvailable_places(int available_places) {
                this.available_places = available_places;
            }

            public String getRoom_type() {
                return room_type;
            }

            public void setRoom_type(String room_type) {
                this.room_type = room_type;
            }

            public Boolean getCan_rent_separately() {
                return can_rent_separately;
            }

            public void setCan_rent_separately(Boolean can_rent_separately) {
                this.can_rent_separately = can_rent_separately;
            }
        }

        public Rooms[] getRooms() {
            return rooms;
        }

        public void setRooms(Rooms[] rooms) {
            this.rooms = rooms;
        }
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
