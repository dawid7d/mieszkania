package com.example.mieszkania.mieszkania.User;


import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.Availabilities;
import com.example.mieszkania.mieszkania.R;
import com.example.mieszkania.mieszkania.Requests.GetAccessoriesRequest;
import com.example.mieszkania.mieszkania.Requests.PostAddRoomAccessoriesRequest;
import com.example.mieszkania.mieszkania.Requests.PostAddRoomRequest;
import com.example.mieszkania.mieszkania.Responses.AccessoryDescyriptionByIdResponse;
import com.example.mieszkania.mieszkania.Responses.AccessoryResponse;
import com.example.mieszkania.mieszkania.Responses.GetRoomTypes;
import com.example.mieszkania.mieszkania.Responses.PostAddRoomAccessoriesResponse;
import com.example.mieszkania.mieszkania.Responses.PostAddRoomResponse;
import com.example.mieszkania.mieszkania.Tasks.AccessoryDescriptionByIdTask;
import com.example.mieszkania.mieszkania.Tasks.GetAccessoriesTask;
import com.example.mieszkania.mieszkania.Tasks.GetRoomTypeTask;
import com.example.mieszkania.mieszkania.Tasks.PostAddRoomAccessoriesTask;
import com.example.mieszkania.mieszkania.Tasks.PostAddRoomTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserAddRooms extends AppCompatActivity {

    Integer room_number;
    ArrayList<String> room_types_list;
    ListView listView,listView2;

    ArrayList<String> Accessories;
    Map<Integer,List<String>> Accessories_Description;
    Map<Integer,List<Integer>> Accessories_Desc_Id;

    CustomAdapter2 customAdapter2;

    Map<Integer, Integer> room_id = new HashMap<>();
    Integer flat_id;

    Map<Integer,Integer> accessory_description = new HashMap<>(); //accessory_id , accessory_description_id
    Map<Integer,String> user_description = new HashMap<>(); //accessory_id , "description"[String, default = ""]
    ArrayList<Integer> Accessory_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_add_rooms);

        Bundle extras = getIntent().getExtras();
        room_number = Integer.parseInt(extras.getString("rooms_number"));
        flat_id = Integer.parseInt(extras.getString("flat_id"));
        Log.v("ave rooms number = ", String.valueOf(room_number));
        Log.v("ave flat id = ", String.valueOf(flat_id));

        room_types_list = new ArrayList<>();
        GetRoomTypes getRoomTypes = GetRoomTypeTask.unpack(new GetRoomTypeTask().execute());
        for (String roomType :
                getRoomTypes.getData().getRoom_types()) {
            if(!roomType.equals(GetAccessoriesRequest.FLAT)){
                room_types_list.add(roomType);
            }
        }

        CustomAdapter customAdapter = new CustomAdapter();
        listView = findViewById(R.id.add_rooms_list);
        listView.setAdapter(customAdapter);

        Button finish = findViewById(R.id.finish);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserAddRooms.this, UserFlats.class);
                startActivity(intent);
            }
        });
    }

    class CustomAdapter extends BaseAdapter {

        Map<Integer, Integer> rooms_map = new HashMap<>();
        String choosed_room_type;
        List<Integer> accessory_button = new ArrayList<>(Collections.nCopies(room_number, 0));

        @Override
        public int getCount() {
            return room_number;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.add_room_layout,null);

            final Context context = viewGroup.getContext();
            
            final Spinner room_types_spinner = view.findViewById(R.id.room_type_spinner);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                    android.R.layout.simple_spinner_dropdown_item,room_types_list);
            room_types_spinner.setAdapter(adapter);

            if (rooms_map.containsKey(i)) {
                room_types_spinner.setSelection(rooms_map.get(i));
            }

            room_types_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    rooms_map.put(i,position);
                }
                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                }
            });

            Button add_room = view.findViewById(R.id.room);
            add_room.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    final View view1 = getLayoutInflater().inflate(R.layout.add_new_room_dialog,null);
                    builder.setView(view1);
                    final AlertDialog dialog = builder.create();
                    dialog.show();

                    choosed_room_type = room_types_spinner.getSelectedItem().toString();

                    TextView room_type = view1.findViewById(R.id.room_type);
                    room_type.setText(choosed_room_type);
                    final EditText yardage = view1.findViewById(R.id.yardage);
                    final EditText total_places = view1.findViewById(R.id.total_places);
                    final CheckBox checkBox = view1.findViewById(R.id.checkBox2);

                    total_places.setVisibility(View.VISIBLE);
                    checkBox.setVisibility(View.VISIBLE);

                    //TODO: change filtration by room type
                    if(choosed_room_type.equals("living_room")){
                        total_places.setVisibility(View.GONE);
                        checkBox.setVisibility(View.GONE);
                    }

                    Button save = view1.findViewById(R.id.save);
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            accessory_button.set(i,1);
                            notifyDataSetChanged();
                            //#// Post - Dodanie pomieszczenia
                            String room_type = room_types_spinner.getSelectedItem().toString();
                            Double yardage_value = Double.parseDouble(yardage.getText().toString());
                            Integer total_places_value = choosed_room_type.equals("living_room") ? 0 : Integer.parseInt(total_places.getText().toString());
                            Log.v("map post","flat id = "+flat_id);
                            Log.v("map post","room_type = "+ room_type);
                            Log.v("map post", "yardage = "+yardage.getText());
                            Log.v("map post","total_places = "+total_places.getText());
                            Log.v("map post","can_rent_separately = "+checkBox.isChecked());
                            PostAddRoomRequest request = new PostAddRoomRequest();
                            request.getRoom().setRoom_type(room_type);
                            request.getRoom().setYardage(yardage_value);
                            request.getRoom().setTotal_places(total_places_value);
                            request.getRoom().setCan_rent_separately(checkBox.isChecked());

                            PostAddRoomTask.ParamsStruct paramsStruct = new PostAddRoomTask.ParamsStruct();
                            paramsStruct.setFlatId(flat_id);
                            paramsStruct.setRequest(request);
                            PostAddRoomResponse response = PostAddRoomTask.unpack(new PostAddRoomTask().execute(paramsStruct));
                            Log.v("PostAddRoomResponse", "success = " + response.isSuccess());
                            int roomId = response.getData().getRoom().getId();
                            room_id.put(i,roomId);
                            dialog.dismiss();

                        }
                    });
                }
            });

            Button add_room_accessories = view.findViewById(R.id.add_room_accessories);
            add_room_accessories.setVisibility(View.GONE);
            if(accessory_button.get(i)==1){
                add_room_accessories.setVisibility(View.VISIBLE);
            }
            add_room_accessories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    final View view1 = getLayoutInflater().inflate(R.layout.add_room_accessories_dialog,null);
                    builder.setView(view1);
                    final AlertDialog dialog = builder.create();
                    dialog.show();

                    //TODO: Get accessories_list by room_type (create GetAccessoriesRequest)
                    Accessories = new ArrayList<>();
                    Accessory_id = new ArrayList<>();
                    AccessoryResponse accessoryResponse = GetAccessoriesTask.unpack(new GetAccessoriesTask().execute(choosed_room_type));
                    for (AccessoryResponse.Data.Accessory a : accessoryResponse.getData().getAccessories()){
                        Accessories.add(a.getName());
                        Accessory_id.add(a.getId());
                        accessory_description.put(a.getId(),1);
                        user_description.put(a.getId(),"");
                    }

                    Accessories_Description = new HashMap<>();
                    Accessories_Desc_Id = new HashMap<>();
                    for (AccessoryResponse.Data.Accessory a : accessoryResponse.getData().getAccessories()){
                        AccessoryDescyriptionByIdResponse accessoryDescyription =
                                AccessoryDescriptionByIdTask.unpack(new AccessoryDescriptionByIdTask().execute(new Integer(a.getId())));

                        List<String> temp = new ArrayList();
                        List<Integer> temp_id = new ArrayList();
                        for (AccessoryDescyriptionByIdResponse.Data.Accessory_Descriptions aa : accessoryDescyription.getData().getAccessory_descriptions()){
                            accessory_description.put(a.getId(),aa.getId());
                            temp.add(aa.getName());
                            temp_id.add(aa.getId());
                        }
                        Accessories_Description.put(a.getId(),temp);
                        Accessories_Desc_Id.put(a.getId(),temp_id);
                    }

                    customAdapter2 = new CustomAdapter2();
                    listView2 = view1.findViewById(R.id.room_accessories_list);
                    listView2.setAdapter(customAdapter2);

                    Button save = view1.findViewById(R.id.save);
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //TODO: send accessories to room in DB
                            //#// Dodanie nowych cech do pomieszczenia
                            Log.v("map post 2","flat id = "+flat_id);
                            Log.v("map post 2","room id = "+room_id.get(i));
                            Log.v("map","---------------------------------------------------------------------");
                            Map<Integer, PostAddRoomAccessoriesRequest> requests = new HashMap<>();
                            for (Map.Entry<Integer, Integer> entry : accessory_description.entrySet()) {
                                Log.v("map acc_dec","acc_id="+entry.getKey() + ": acc_desc_id=" + entry.getValue().toString());
                                requests.put(entry.getKey(), new PostAddRoomAccessoriesRequest());
                                requests.get(entry.getKey())
                                        .getDescribed_accessory()
                                        .setAccessory_id(entry.getKey());
                                requests.get(entry.getKey())
                                        .getDescribed_accessory()
                                        .setAccessory_description_id(entry.getValue());
                            }
                            Log.v("map","---------------------------------------------------------------------");
                            for (Map.Entry<Integer, String> entry : user_description.entrySet()) {
                                Log.v("map user_dec","acc_id="+entry.getKey() + ": user_desc=" + entry.getValue().toString());
                                requests.get(entry.getKey())
                                        .getDescribed_accessory()
                                        .setDescription(entry.getValue());
                            }

                            for (Map.Entry<Integer, PostAddRoomAccessoriesRequest> e :
                                    requests.entrySet()) {
                                PostAddRoomAccessoriesRequest request = e.getValue();
                                PostAddRoomAccessoriesTask.ParamStruct paramStruct = new PostAddRoomAccessoriesTask.ParamStruct();
                                paramStruct.setRequest(request);
                                paramStruct.setRoomId(room_id.get(i));
                                paramStruct.setFlatId(flat_id);

                                PostAddRoomAccessoriesResponse response =
                                        PostAddRoomAccessoriesTask.unpack(new PostAddRoomAccessoriesTask().execute(paramStruct));

                                Log.v("PostAddRoomAccResp", "success=" + response.isSuccess());
                            }
                            dialog.dismiss();
                        }
                    });
                }
            });
            return view;
        }
    }


    class CustomAdapter2 extends BaseAdapter {

        Map<Integer, Integer> room_accessory_map = new HashMap<>();
        Map<Integer, Boolean> checkbox_map = new HashMap<>();

        @Override
        public int getCount() {
            return Accessories.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.attribute_layout,null);

            TextView accessory_name = view.findViewById(R.id.accessory_name);
            accessory_name.setText(Accessories.get(i));

            final Context context = viewGroup.getContext();

            Spinner accessory_desc_spinner = view.findViewById(R.id.accessory_descriptions);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                    android.R.layout.simple_spinner_dropdown_item,Accessories_Description.get(Accessory_id.get(i)));
            accessory_desc_spinner.setAdapter(adapter);

            if (room_accessory_map.containsKey(i)) {
                accessory_desc_spinner.setSelection(room_accessory_map.get(i));
            }

            accessory_desc_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    room_accessory_map.put(i,position);
                    accessory_description.put(Accessory_id.get(i), Accessories_Desc_Id.get(Accessory_id.get(i)).get(position));
                    Log.v("ave","accesory id="+Accessory_id.get(i)+" position="+position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                }
            });

            CheckBox checkBox = view.findViewById(R.id.checkBox);
            if(checkbox_map.containsKey(i)){
                checkBox.setChecked(checkbox_map.get(i));
            }
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    checkbox_map.put(i,Boolean.TRUE);
                }
            });

            Button add_description = view.findViewById(R.id.user_description);
            add_description.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    View view1 = getLayoutInflater().inflate(R.layout.add_accessory_description_dialog,null);
                    builder.setView(view1);
                    final AlertDialog dialog = builder.create();
                    dialog.show();

                    final EditText user_description_added = view1.findViewById(R.id.user_description);
                    user_description_added.getText();

                    Button ok = view1.findViewById(R.id.ok);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //TODO: get user description
                            user_description.put(Accessory_id.get(i), String.valueOf(user_description_added.getText()));
                            dialog.dismiss();
                        }
                    });
                }
            });
            return view;
        }
    }
}
