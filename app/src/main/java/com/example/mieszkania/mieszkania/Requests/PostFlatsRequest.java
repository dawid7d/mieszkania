package com.example.mieszkania.mieszkania.Requests;

/**
 * Created by pflis on 19.05.2018.
 */

public class PostFlatsRequest {
    Flat flat;
    public PostFlatsRequest(){
        flat = new Flat();
    }

    public Flat getFlat() {
        return flat;
    }

    public static class Flat{

        public Flat(){
        }

        Double additional_rent_cost;
        String address_building_number;
        String address_city;
        String address_flat_number;
        String address_street;
        String building_type;
        Integer building_year;
        Integer floor;
        Double media_cost;
        Integer rooms_number;
        String windows_type;
        Double yardage;

        public Double getAdditional_rent_cost() {
            return additional_rent_cost;
        }

        public Double getMedia_cost() {
            return media_cost;
        }

        public Double getYardage() {
            return yardage;
        }

        public String getAddress_building_number() {
            return address_building_number;
        }

        public Integer getBuilding_year() {
            return building_year;
        }

        public Integer getFloor() {
            return floor;
        }

        public Integer getRooms_number() {
            return rooms_number;
        }

        public String getAddress_city() {
            return address_city;
        }

        public String getAddress_flat_number() {
            return address_flat_number;
        }

        public String getAddress_street() {
            return address_street;
        }

        public String getBuilding_type() {
            return building_type;
        }

        public String getWindows_type() {
            return windows_type;
        }

        public void setAdditional_rent_cost(Double additional_rent_cost) {
            this.additional_rent_cost = additional_rent_cost;
        }

        public void setAddress_building_number(String address_building_number) {
            this.address_building_number = address_building_number;
        }

        public void setAddress_city(String address_city) {
            this.address_city = address_city;
        }

        public void setAddress_flat_number(String address_flat_number) {
            this.address_flat_number = address_flat_number;
        }

        public void setAddress_street(String address_street) {
            this.address_street = address_street;
        }

        public void setBuilding_type(String building_type) {
            this.building_type = building_type;
        }

        public void setBuilding_year(Integer building_year) {
            this.building_year = building_year;
        }

        public void setFloor(Integer floor) {
            this.floor = floor;
        }

        public void setMedia_cost(Double media_cost) {
            this.media_cost = media_cost;
        }

        public void setRooms_number(Integer rooms_number) {
            this.rooms_number = rooms_number;
        }

        public void setWindows_type(String windows_type) {
            this.windows_type = windows_type;
        }

        public void setYardage(Double yardage) {
            this.yardage = yardage;
        }
    }
}
