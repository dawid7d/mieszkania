package com.example.mieszkania.mieszkania.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.R;
import com.example.mieszkania.mieszkania.Responses.LoginResponse;
import com.example.mieszkania.mieszkania.Responses.PersonalDataResponse;
import com.example.mieszkania.mieszkania.Tasks.LoginTask;
import com.example.mieszkania.mieszkania.Tasks.PersonalDataTask;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class LoginActivity extends AppCompatActivity {

    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login = findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = ((EditText) findViewById(R.id.name)).getText().toString();
                UserState.setEmail(email);
                String password = ((EditText) findViewById(R.id.password)).getText().toString();

                AsyncTask<String, Void, ResponseEntity<LoginResponse>> loginResponseAsyncTask = new LoginTask().execute(email, password);
                try {
                    ResponseEntity<LoginResponse> loginResponseResponseEntity = loginResponseAsyncTask.get();


                    LoginResponse loginResponse = loginResponseResponseEntity.getBody();
                    if(loginResponse.isSuccess()){
                        List<String> list = loginResponseResponseEntity.getHeaders().get("api-key");
                        if(list != null && list.size()>0){
                            String newApiKey = list.get(0);
                            Log.v("loginResponse:", "new api key = {" + newApiKey + "}");
                            UserState.setNextApiKey(newApiKey); //remember new api-key to use in next requests
                            UserState.setIsLogged(true);

                            Intent intent = new Intent(LoginActivity.this,AplicationActivity.class);
                            startActivity(intent);
                        }
                        else {
                            Log.v("loginResponse", "no api key in http response");
                        }
                    }

                    Log.v("response:", loginResponse.toString());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                AsyncTask<Void, Void, ResponseEntity<PersonalDataResponse>> entityAsyncTask =  new PersonalDataTask().execute();

                try{
                    ResponseEntity<PersonalDataResponse> responseResponseEntity =  entityAsyncTask.get();
                    PersonalDataResponse personalDataResponse = responseResponseEntity.getBody();
                    if(personalDataResponse.getSuccess()){
                        List<String> list = responseResponseEntity.getHeaders().get("api-key");
                        if(list != null && list.size()>0){
                            String newApiKey = list.get(0);
                            Log.v("PersonalDataResponse:", "new api key = {" + newApiKey + "}");
                            UserState.setNextApiKey(newApiKey); //remember new api-key to use in next requests
                            UserState.setFirst_name(personalDataResponse.getData().getUser().getFirst_name());
                            UserState.setLast_name(personalDataResponse.getData().getUser().getLast_name());
                            UserState.setPhone_number(personalDataResponse.getData().getUser().getPhone_number());
                        }
                        else {
                            Log.v("PersonalDataResponse", "no api key in http response");
                        }
                    }
                    else{
                        Log.v("PersonalDataResponse", "success == false");
                    }
                }
                catch (Exception e){
                    Log.v("PersonalDataResponse", e.toString());
                }

                Intent intent = new Intent(LoginActivity.this,AplicationActivity.class);
                startActivity(intent);
            }
        });

    }

}
