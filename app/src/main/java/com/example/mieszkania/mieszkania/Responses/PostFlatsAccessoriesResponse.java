package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostFlatsAccessoriesResponse {

    public PostFlatsAccessoriesResponse(){
    }

    boolean success;
    Data data;

    public Data getData() {
        return data;
    }

    public boolean isSuccess() {
        return success;
    }
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data{
        public static class DescribedAccessory{

            public DescribedAccessory(){
            }
            Integer id;
            Integer accessory_id;
            Integer accessory_description_id;
            String description;
            String created_at;
            String updated_at;
            String accessory_name;
            String accessory_description_name;

            public String getDescription() {
                return description;
            }

            public Integer getAccessory_description_id() {
                return accessory_description_id;
            }

            public Integer getAccessory_id() {
                return accessory_id;
            }

            public void setAccessory_description_id(Integer accessory_description_id) {
                this.accessory_description_id = accessory_description_id;
            }

            public void setAccessory_id(Integer accessory_id) {
                this.accessory_id = accessory_id;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }
        }
        DescribedAccessory described_accessory;
        public DescribedAccessory getDescribed_accessory() {
            return described_accessory;
        }
    }




}

