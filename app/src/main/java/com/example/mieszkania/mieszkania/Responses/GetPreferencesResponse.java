package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class        GetPreferencesResponse {
    Boolean success;
    Data data;

    public static class Data {

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class User{
            String place;
            Double distance;
            Double city_travel_time;
            Double car_travel_time;
            String place_lat;
            String place_lng;

            public Double getCar_travel_time() {
                return car_travel_time;
            }

            public Double getCity_travel_time() {
                return city_travel_time;
            }

            public Double getDistance() {
                return distance;
            }

            public String getPlace() {
                return place;
            }
        }
        User user;

        public User getUser() {
            return user;
        }
    }

    public Boolean getSuccess() {
        return success;
    }

    public Data getData() {
        return data;
    }
}
