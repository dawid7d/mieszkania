package com.example.mieszkania.mieszkania.Requests;

public class PostAddRoomAccessoriesRequest {
    public PostAddRoomAccessoriesRequest(){
        described_accessory = new DescribedAccessory();
    }
    public static class DescribedAccessory{

        public DescribedAccessory(){
            accessory_description_id = new Integer(0);
            accessory_id = new Integer(0);
            description = new String();
        }

        Integer accessory_id;
        Integer accessory_description_id;
        String description;

        public String getDescription() {
            return description;
        }

        public Integer getAccessory_description_id() {
            return accessory_description_id;
        }

        public Integer getAccessory_id() {
            return accessory_id;
        }

        public void setAccessory_description_id(Integer accessory_description_id) {
            this.accessory_description_id = accessory_description_id;
        }

        public void setAccessory_id(Integer accessory_id) {
            this.accessory_id = accessory_id;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
    DescribedAccessory described_accessory;

    public DescribedAccessory getDescribed_accessory() {
        return described_accessory;
    }
}
