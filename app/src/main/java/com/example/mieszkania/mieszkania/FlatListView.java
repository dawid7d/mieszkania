package com.example.mieszkania.mieszkania;


import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class FlatListView extends ArrayAdapter<String> {

    private Activity context;
    private Integer[] images;
    private String[] price, description, date;
    private ArrayList<Integer> id;

    public FlatListView(Activity context, Integer[] img, String[] description, String[] price, String[] date, ArrayList<Integer> id) {
        super(context, R.layout.flat_layout, description);
        this.context = context;
        this.description = description;
        this.images = img;
        this.price = price;
        this.date = date;
        this.id = id;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View r = convertView;
        ViewHolder viewHolder;
        if(r==null){
            LayoutInflater layoutInflater = context.getLayoutInflater();
            r = layoutInflater.inflate(R.layout.flat_layout,null,true);
            viewHolder = new ViewHolder(r);
            r.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) r.getTag();
        }
        viewHolder.imageView.setImageResource(images[position]);
        viewHolder.title.setText(description[position]);
        viewHolder.price.setText(price[position]);
        viewHolder.date.setText(date[position]);
        viewHolder.id.setText("id: "+id.get(position).toString());

        return r;
    }

    private class ViewHolder {
        TextView title, price, date, id;
        ImageView imageView;
        ViewHolder(View v){
            title =  v.findViewById(R.id.title);
            price = v.findViewById(R.id.price);
            date =  v.findViewById(R.id.start_end_date);
            imageView = v.findViewById(R.id.imageView);
            id = v.findViewById(R.id.id);
        }
    }
}
