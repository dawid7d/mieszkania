package com.example.mieszkania.mieszkania;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.Activities.AplicationActivity;
import com.example.mieszkania.mieszkania.Responses.AdvertisementsResponse;
import com.example.mieszkania.mieszkania.Responses.FlatByIdResponse;
import com.example.mieszkania.mieszkania.Responses.FlatsResponse;
import com.example.mieszkania.mieszkania.Responses.GetRoomByIdResponse;
import com.example.mieszkania.mieszkania.Tasks.GetAddsByPreferencesTask;
import com.example.mieszkania.mieszkania.Tasks.GetFlatByIdTask;
import com.example.mieszkania.mieszkania.Tasks.GetFlatsListTask;
import com.example.mieszkania.mieszkania.Tasks.GetRoomByIdTask;
import com.example.mieszkania.mieszkania.User.UserState;

import java.util.ArrayList;

public class ProposedOffers extends AppCompatActivity {


    ListView listView2;

    static ArrayList<FlatByIdResponse.Data.Flat> flats_list;
    static ArrayList<AdvertisementsResponse.Data.Advertisements> advertisements_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proposed_offers);



        //TODO: get data form DataBase
        Integer[] img = {R.drawable.img_3, R.drawable.img_4};
        ArrayList<Integer> id = new ArrayList<>();
        for(int i=1; i<=img.length; i++){id.add(i);}
//        String[] descriotion = {"Mieszkanie 2 pokoje + kuchnia", "Mieszkanie dla studentów"};
//        String[] price = {"599 zł", "999 zł"};
//        String[] date = {"03.04.2018 - 03.05.2018", "03.04.2018 - 03.05.2018"};

        advertisements_list = new ArrayList<>();
        flats_list = new ArrayList<>();

        AdvertisementsResponse advertisementsResponse = GetAddsByPreferencesTask.unpack(new GetAddsByPreferencesTask().execute());
        for(AdvertisementsResponse.Data.Advertisements a : advertisementsResponse.getData().getAdvertisement()){
            Log.v("ave","id=" + a.getId()+
                    " user id=" + a.getUser_id()+
                    " target id= " + a.getTarget_id()+
                    " target type= " + a.getTarget_type()+
                    " rental_price" + a.getRental_price()+
                    " start_data" + a.getStart_rent_data()+
                    " end_data" + a.getEnd_rent_data()+
                    " contract type" + a.getContract_type());
            advertisements_list.add(a);
        }

//        final ListView listView2 = findViewById(R.id.flatsListView);
//        FlatListView flatListView2 = new FlatListView(this, img, descriotion, price, date, id);
//        listView2.setAdapter(flatListView2);
//        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//
//
//                Log.v("getItemIdAtPosition(): ", String.valueOf(listView2.getItemIdAtPosition(i)))   ;
//                Intent intent = new Intent(ProposedOffers.this,Flat.class);
//                intent.putExtra("is_login", UserState.getIsLogged());
//                startActivity(intent);
//            }
//        });




        FlatsResponse flatsResponse = GetFlatsListTask.unpack(new GetFlatsListTask().execute());
        for(FlatsResponse.Data.Flat f : flatsResponse.getData().getFlats()) {
            Log.v("ave","flat id = "+f.getId());
            Log.v("FlatsResponse", String.valueOf(f.getId()));
        }
        for(FlatsResponse.Data.Flat f : flatsResponse.getData().getFlats()){
            FlatByIdResponse flatByIdResponse =  GetFlatByIdTask.unpack(new GetFlatByIdTask().execute(new Integer(f.getId())));
            flats_list.add(flatByIdResponse.getData().getFlat());
            Log.v("FlatByIdResponse", flatByIdResponse.getData().getFlat().getAddress_city());
        }
        for(FlatByIdResponse.Data.Flat f : flats_list ){
            Log.v("ave","flat id=" + f.getId()+
                    " Media_cost=" + f.getMedia_cost()+
                    " flat Yardage=" + f.getYardage()+
                    " Rooms_number=" + f.getRooms_number()+
                    " flat floor=" + f.getFloor()+
                    " city="+f.getAddress_city()+
                    " street="+f.getAddress_street());
        }

        AdvertisementsAdapter advertisementsAdapter = new AdvertisementsAdapter(advertisements_list);
        listView2 = findViewById(R.id.flatsListView);
        listView2.setAdapter(advertisementsAdapter);
        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.v("getItemIdAtPosition(): ", String.valueOf(listView2.getItemIdAtPosition(i)));
                Log.v("ave", "pozycja na liscie="+advertisements_list.get(i).getId());

                if(advertisements_list.get(i).getTarget_type().equals("Flat")){
                    Log.v("ave", "flat target id to GET="+advertisements_list.get(i).getTarget_id());
                    int selectedFlatId = advertisements_list.get(i).getTarget_id();
                    FlatByIdResponse flatByIdResponse = GetFlatByIdTask.unpack(new GetFlatByIdTask().execute(new Integer(selectedFlatId)));

                    Intent intent = new Intent(ProposedOffers.this, Flat.class);
                    UserState.setCanEdit(false);
                    intent.putExtra("can_edit", UserState.getCanEdit());
                    intent.putExtra("Flat", flatByIdResponse.getData().getFlat());
                    intent.putExtra("Advertisement", advertisements_list.get(i));
                    startActivity(intent);
                } else {
                    Log.v("ave", "room target id to GET="+advertisements_list.get(i).getTarget_id());
                    int selectedRoomId = advertisements_list.get(i).getTarget_id();
                    GetRoomByIdResponse RoomByIdResponse = GetRoomByIdTask.unpack(new GetRoomByIdTask().execute(new Integer(selectedRoomId)));
                    GetRoomByIdResponse.Data.Room room = RoomByIdResponse.getData().getRoom();
                    Intent intent = new Intent(ProposedOffers.this,Room.class);
                    UserState.setCanEdit(false);
                    intent.putExtra("can_edit", UserState.getCanEdit());
                    intent.putExtra("RoomById", RoomByIdResponse.getData().getRoom());
                    intent.putExtra("flat_id", RoomByIdResponse.getData().getRoom().getFlat_id());
                    startActivity(intent);
                }
            }
        });

    }
    public class AdvertisementsAdapter extends BaseAdapter {
        ArrayList<AdvertisementsResponse.Data.Advertisements> advertisements_list;

        AdvertisementsAdapter(ArrayList<AdvertisementsResponse.Data.Advertisements> _advertisements_list){
            this.advertisements_list = _advertisements_list;
        }

        @Override
        public int getCount() {
            return advertisements_list.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.flat_adapter_layout,null);

            TextView textView = view.findViewById(R.id.textView);
            String ad_active = "";
            if(advertisements_list.get(i).isIs_active()){ad_active="Tak";}
            else {ad_active="Nie";}
            textView.setText(" \n"+advertisements_list.get(i).getTitle()+"\n\n"+
                    " Koszt miesieczny: "+advertisements_list.get(i).getRental_price()+"zł \n"+
                    " Zaliczka: "+advertisements_list.get(i).getDeposit()+"zł\n\n"+
                    " Czy aktywne: "+ad_active+"\n\n"+
                    " Od: "+advertisements_list.get(i).getStart_rent_data()+"\n"+
                    " Do: "+advertisements_list.get(i).getEnd_rent_data()+"\n"
            );

            return view;
        }
    }
}

