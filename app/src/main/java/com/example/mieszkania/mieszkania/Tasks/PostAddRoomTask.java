package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Requests.PostAddRoomRequest;
import com.example.mieszkania.mieszkania.Requests.PostFlatsAccessoriesRequest;
import com.example.mieszkania.mieszkania.Responses.PostAddRoomResponse;
import com.example.mieszkania.mieszkania.Responses.PostFlatsAccessoriesResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class PostAddRoomTask extends AsyncTask<PostAddRoomTask.ParamsStruct, Void, ResponseEntity<PostAddRoomResponse>> {

    public static class ParamsStruct{
        PostAddRoomRequest request;
        Integer flatId;

        public Integer getFlatId() {
            return flatId;
        }

        public PostAddRoomRequest getRequest() {
            return request;
        }

        public void setFlatId(Integer flatId) {
            this.flatId = flatId;
        }

        public void setRequest(PostAddRoomRequest request) {
            this.request = request;
        }
    }

    @Override
    protected ResponseEntity<PostAddRoomResponse> doInBackground(ParamsStruct ... requests) {
        ParamsStruct input = requests[0];
        PostAddRoomRequest objToPass = input.getRequest();
        Integer flatId = input.getFlatId();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("PostAddRoomTask", "sending PostAddRoomTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<PostAddRoomRequest> request = new HttpEntity<>(objToPass, headers);
        String url = Config.getHost() + Config.getFlatRoomsEndpoint(flatId);

        ResponseEntity<PostAddRoomResponse> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                request,
                PostAddRoomResponse.class);
        return responseEntity;
    }

    public static PostAddRoomResponse unpack(AsyncTask<PostAddRoomTask.ParamsStruct, Void, ResponseEntity<PostAddRoomResponse>>   asyncTask)
    {
        try{
            ResponseEntity<PostAddRoomResponse> responseEntity = asyncTask.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                PostAddRoomResponse response  = responseEntity.getBody();
                if(response != null) {
                    return response;
                }
                else {
                    Log.v("PostAddRoomResponse", "PostAddRoomResponse == null");
                    return null;
                }
            }
            else{
                Log.v("PostAddRoomResponse", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("PostAddRoomResponse", e.toString());
        }
        return null;
    }

}
