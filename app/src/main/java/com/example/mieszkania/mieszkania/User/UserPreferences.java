package com.example.mieszkania.mieszkania.User;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.R;
import com.example.mieszkania.mieszkania.Requests.ChangePreferencesRequest;
import com.example.mieszkania.mieszkania.Responses.ChangePreferencesResponse;
import com.example.mieszkania.mieszkania.Responses.GetPreferencesResponse;
import com.example.mieszkania.mieszkania.Tasks.ChangePreferencesTask;
import com.example.mieszkania.mieszkania.Tasks.GetPreferencesTask;

public class UserPreferences extends AppCompatActivity {

    TextView distanceTextView;
    TextView placeTextView;
    TextView city_travelTextView;
    TextView car_travelTextView;
    Button change_preferences;

    Double distance;
    String place;
    Double city_travel;
    Double car_travel;

    EditText _distance;
    EditText _place;
    EditText _city_travel;
    EditText _car_travel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_preferences);

        distanceTextView =  findViewById(R.id.distance);
        placeTextView =  findViewById(R.id.place);
        city_travelTextView = findViewById(R.id.city_travel);
        car_travelTextView = findViewById(R.id.car_travel);

        //TODO: GET preferences from DB
        GetPreferencesResponse  getPreferencesResponse = GetPreferencesTask.unpack(new GetPreferencesTask().execute());
        distance = getPreferencesResponse.getData().getUser().getDistance();
        place = getPreferencesResponse.getData().getUser().getPlace();
        city_travel = getPreferencesResponse.getData().getUser().getCity_travel_time();
        car_travel = getPreferencesResponse.getData().getUser().getCar_travel_time();

        placeTextView.setText("Miejsce: " + place);
        distanceTextView.setText("Odległość od miejsca: " + Double.toString(distance));
        city_travelTextView.setText("Czas dojazdu komunikacją: " + Double.toString(city_travel));
        car_travelTextView.setText("Czas dojazdu samochodem: " +Double.toString(car_travel));

        change_preferences = findViewById(R.id.change_preferences);
        change_preferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(UserPreferences.this);
                View view1 = getLayoutInflater().inflate(R.layout.user_edit_preferences,null);
                builder.setView(view1);
                final AlertDialog dialog = builder.create();
                dialog.show();

                _place = view1.findViewById(R.id._new_name);
                _distance = view1.findViewById(R.id._distance);
                _city_travel = view1.findViewById(R.id._new_last_name);
                _car_travel = view1.findViewById(R.id._new_phone);
                _place.setText(place);
                _distance.setText(String.valueOf(distance));
                _city_travel.setText(String.valueOf(city_travel));
                _car_travel.setText(String.valueOf(car_travel));


                Button save = view1.findViewById(R.id._save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ChangePreferencesRequest changePreferencesRequest = new ChangePreferencesRequest();
                        changePreferencesRequest.getCustomer().setPlace(_place.getText().toString());
                        changePreferencesRequest
                                .getCustomer()
                                .setCar_travel_time(Double.parseDouble(_car_travel.getText().toString()));
                        changePreferencesRequest
                                .getCustomer()
                                .setCity_travel_time(Double.parseDouble(_city_travel.getText().toString()));
                        changePreferencesRequest
                                .getCustomer()
                                .setDistance(Double.parseDouble(_distance.getText().toString()));

                        ChangePreferencesResponse changePreferencesResponse =  ChangePreferencesTask.unpack(new ChangePreferencesTask().execute(changePreferencesRequest));
                        dialog.dismiss();
                    }
                });

            }
        });
    }
}
