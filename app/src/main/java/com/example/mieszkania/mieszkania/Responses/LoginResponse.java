package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse {
    boolean success;

    LoginResponse(){
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String toString(){
        return "LoginResponse{"+
                "success=" + this.success +
                '}';
    }
}
