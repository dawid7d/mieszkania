package com.example.mieszkania.mieszkania;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.Requests.PostAdvertisementRequest;
import com.example.mieszkania.mieszkania.Responses.GetFlatRoomsResponse;
import com.example.mieszkania.mieszkania.Responses.GetRoomAccessoriesResponse;
import com.example.mieszkania.mieszkania.Responses.GetRoomByIdResponse;
import com.example.mieszkania.mieszkania.Responses.PostAdvertisementResponse;
import com.example.mieszkania.mieszkania.Tasks.GetRoomAccessoriesTask;
import com.example.mieszkania.mieszkania.Tasks.PostAdvertisementTask;

import java.util.ArrayList;

public class Room extends AppCompatActivity {

    TextView room_information,equipment;
    Button room_photo,room_edit,delete_room,add_room_advert;
    Integer[] img;
    int flat_id;
    Boolean can_edit=false;

    GetFlatRoomsResponse.Data.Rooms this_room;
    GetRoomByIdResponse.Data.Room this_room_by_id;
    ArrayList<String> accessories_list;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            can_edit = extras.getBoolean("can_edit");
            this_room = (GetFlatRoomsResponse.Data.Rooms) getIntent().getSerializableExtra("Room");
            this_room_by_id = (GetRoomByIdResponse.Data.Room) getIntent().getSerializableExtra("RoomById");
            flat_id = extras.getInt("flat_id");
        }

        room_edit = findViewById(R.id.room_edit);
        delete_room = findViewById(R.id.delete_room);
        add_room_advert = findViewById(R.id.add_room_advert);

        if(can_edit){
            room_edit.setVisibility(View.VISIBLE);
            delete_room.setVisibility(View.VISIBLE);
            add_room_advert.setVisibility(View.VISIBLE);
        }else {
            room_edit.setVisibility(View.GONE);
            delete_room.setVisibility(View.GONE);
            add_room_advert.setVisibility(View.GONE);
        }

        room_information = findViewById(R.id.room_information);
        if(this_room != null){
            room_information.setText(
                    "Informacje dot. pomieszczenia: \n\n"+
//                            "Typ pomieszczenia: " + this_room.getRoom_type()+"\n"+
                            "Wolne miejsca: " +  this_room.getAvailable_places()+"\n"+
                            "Wszystkie miejsca: " +  this_room.getTotal_places()+"\n"+
                            "Powierzchnia pomieszczenia: " + this_room.getYardage()+" m2\n"+
                            "Czy możliwa umowa osobno: " + this_room.getCan_rent_separately()+"\n");
        } else {
            room_information.setText(
                    "Informacje dot. pomieszczenia: \n\n"+
                            "Wolne miejsca: " +  this_room_by_id.getAvailable_places()+"\n"+
                            "Wszystkie miejsca: " +  this_room_by_id.getTotal_places()+"\n"+
                            "Powierzchnia pomieszczenia: " + this_room_by_id.getYardage()+" m2\n"+
                            "Czy możliwa umowa osobno: " + this_room_by_id.getCan_rent_separately()+"\n");
        }


        room_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Room  .this);
                final View view1 = getLayoutInflater().inflate(R.layout.edit_room_dialog,null);
                builder.setView(view1);
                final AlertDialog dialog = builder.create();
                dialog.show();

                Button save = view1.findViewById(R.id.save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        EditText room_type = view1.findViewById(R.id.room_type);
                        room_type.getText();

                        EditText yardage = view1.findViewById(R.id.yardage);
                        yardage.getText();

                        EditText total_place = view1.findViewById(R.id.total_places);
                        total_place.getText();

                        EditText available_places = view1.findViewById(R.id.available_places);
                        available_places.getText();

                        //TODO: update room in DB

                        dialog.dismiss();
                    }
                });
            }
        });


        //TODO: Take flat image from DB
        img = new Integer[]{R.drawable.img1,R.drawable.img_3, R.drawable.img_6};

        room_photo = findViewById(R.id.room_photos);
        room_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Room.this);
                View view1 = getLayoutInflater().inflate(R.layout.flat_photos_dialog,null);
                builder.setView(view1);
                final AlertDialog dialog = builder.create();
                dialog.show();

                CustomAdapter customAdapter = new CustomAdapter();
                ListView photo_listView = view1.findViewById(R.id.flat_all_photos);
                photo_listView.setAdapter(customAdapter);
            }
        });

        accessories_list = new ArrayList<>();
        GetRoomAccessoriesResponse getRoomAccessoriesResponse;
        if(this_room != null){
            getRoomAccessoriesResponse = GetRoomAccessoriesTask.unpack(new GetRoomAccessoriesTask().execute(flat_id,this_room.getId()));
        } else {
            getRoomAccessoriesResponse = GetRoomAccessoriesTask.unpack(new GetRoomAccessoriesTask().execute(flat_id,this_room_by_id.getId()));
        }

        for(GetRoomAccessoriesResponse.Data.Described_accessories a : getRoomAccessoriesResponse.getData().getDescribed_accessories()){
            Log.v("ave",a.getId()+" "+a.getAccessory_name()+" "+a.getAccessory_description_name());
            accessories_list.add(a.getAccessory_name()+": "+a.getAccessory_description_name()+",  "+a.getDescription()+"\n\n");
        }

        equipment = findViewById(R.id.equipment);
        equipment.append("Akcesoria w pomieszczeniu: \n\n");
        for(String accessory : accessories_list){
            equipment.append(accessory);
        }

        add_room_advert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Room.this);
                final View view2 = getLayoutInflater().inflate(R.layout.add_flat_advert,null);
                builder.setView(view2);
                final AlertDialog dialog = builder.create();
                dialog.show();

                final String[] arraySpinner = new String[] {"czas okreslony", "czas nieokreslony"};
                final Spinner contract_type_spinner =  view2.findViewById(R.id.contract_type_spinner);
                adapter = new ArrayAdapter<>(getApplicationContext(),
                        android.R.layout.simple_spinner_item, arraySpinner);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                contract_type_spinner.setAdapter(adapter);

                final CheckBox checkBox3 = view2.findViewById(R.id.checkBox3);

                final Button save = view2.findViewById(R.id.save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        EditText title_value = view2.findViewById(R.id.title_value);
                        EditText rental_price_value = view2.findViewById(R.id.rental_price_value);
                        EditText deposit_value = view2.findViewById(R.id.deposit_value);
                        EditText start_rent_date = view2.findViewById(R.id.start_value);
                        EditText end_rent_date = view2.findViewById(R.id.end_value);
                        EditText agency_commission = view2.findViewById(R.id.agency_commission_value);
                        boolean is_agency = checkBox3.isChecked();

                        Log.v("##"," target_type=" + "Room\n" +
                                " target_id= " + this_room.getId()+"\n"+
                                " title=" + title_value.getText()+"\n"+
                                " rental_price="+ rental_price_value.getText()+"\n"+
                                " deposit="+ deposit_value.getText()+"\n"+
                                " start_rent_date="+ start_rent_date.getText()+"\n"+
                                " end_rent_date="+ end_rent_date.getText()+"\n"+
                                " contract_type="+ contract_type_spinner.getSelectedItem().toString()+"\n"+
                                " agency_is_checked="+ is_agency+"\n"+
                                " agency_commission="+ agency_commission.getText()+"\n"
                        );
                        PostAdvertisementRequest request = new PostAdvertisementRequest();
                        request.setTarget_type(PostAdvertisementRequest.targetRoom);
                        request.setTarget_id(this_room.getId());
                        request.setTitle(title_value.getText().toString());
                        request.setRental_price(Double.valueOf(rental_price_value.getText().toString()));
                        request.setDeposit(Double.valueOf(deposit_value.getText().toString()));
                        request.setStart_rent_date(start_rent_date.getText().toString());
                        request.setEnd_rent_date(end_rent_date.getText().toString());

                        if(contract_type_spinner.getSelectedItem().toString().equals(arraySpinner[0])){
                            request.setContract_type(PostAdvertisementRequest.indefinite);
                        }else {
                            request.setContract_type(PostAdvertisementRequest.fixedTerm);
                        }

                        request.setAgency(is_agency);
                        if(!is_agency)
                            request.setAgency_commission(0.0);
                        else
                            request.setAgency_commission(Double.valueOf(agency_commission.getText().toString()));

                        PostAdvertisementResponse response = PostAdvertisementTask.unpack(new PostAdvertisementTask().execute(request));
                        Log.v("##","response="+response.isSuccess());
                        dialog.dismiss();
                    }
                });
            }
        });


    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return img.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.photo,null);
            ImageView imageView = view.findViewById(R.id.flat_photo);
            imageView.setImageResource(img[i]);
            return view;
        }
    }
}
