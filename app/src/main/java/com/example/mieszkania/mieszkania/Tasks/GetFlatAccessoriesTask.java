package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.GetFlatAccessoriesResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetFlatAccessoriesTask extends AsyncTask<Integer, Void, ResponseEntity<GetFlatAccessoriesResponse>> {

    @Override
    protected ResponseEntity<GetFlatAccessoriesResponse> doInBackground(Integer ... params) {
        Integer id = params[0];
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetFlatAccessoriesTask", "sending GetFlatAccessoriesTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getFlatsEndpoint() + "/" + String.valueOf(id) + "/" + "described_accessories";
        ResponseEntity<GetFlatAccessoriesResponse> FlatAccessoriesResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetFlatAccessoriesResponse.class);
        return FlatAccessoriesResponseEntity;
    }

    public static GetFlatAccessoriesResponse unpack(AsyncTask<Integer, Void, ResponseEntity<GetFlatAccessoriesResponse>> accessoryResponseResponseEntity)
    {
        try{
            ResponseEntity<GetFlatAccessoriesResponse> responseEntity = accessoryResponseResponseEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                GetFlatAccessoriesResponse accessoryResponse = responseEntity.getBody();
                if(accessoryResponse != null) {
                    return accessoryResponse;
                }
                else {
                    Log.v("GetFlatAccessoriesTask", "GetRoomTypeAccessoriesResponese == null");
                    return null;
                }
            }
            else{
                Log.v("GetFlatAccessoriesTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetFlatAccessoriesTask", e.toString());
        }
        return null;
    }
}
