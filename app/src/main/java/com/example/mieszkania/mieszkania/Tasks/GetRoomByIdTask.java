package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.FlatByIdResponse;
import com.example.mieszkania.mieszkania.Responses.GetRoomByIdResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetRoomByIdTask extends AsyncTask<Integer, Void, ResponseEntity<GetRoomByIdResponse>> {

    @Override
    protected ResponseEntity<GetRoomByIdResponse> doInBackground(Integer ... params) {
        Integer id = params[0];
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetFlatByIdTask", "sending GetFlatByIdTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getRooms() + "/" + String.valueOf(id);
        ResponseEntity<GetRoomByIdResponse> roomsResponseResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetRoomByIdResponse.class);
        return roomsResponseResponseEntity;
    }

    public static GetRoomByIdResponse unpack(AsyncTask<Integer, Void, ResponseEntity<GetRoomByIdResponse>>  roomsByIdResponseEntity)
    {
        try{
            ResponseEntity<GetRoomByIdResponse> responseEntity = roomsByIdResponseEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                GetRoomByIdResponse roomsResponse  = responseEntity.getBody();
                if(roomsResponse != null) {
                    return roomsResponse;
                }
                else {
                    Log.v("GetFlatByIdTask", "GetRoomByIdResponse == null");
                    return null;
                }
            }
            else{
                Log.v("GetFlatsListTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetFlatsListTask", e.toString());
        }
        return null;
    }
}

