package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.GetAvailabilitiesResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetAvailabilitieTask extends AsyncTask<Integer, Void, ResponseEntity<GetAvailabilitiesResponse>> {

    @Override
    protected ResponseEntity<GetAvailabilitiesResponse> doInBackground(Integer ... params) {
        Integer id = params[0];
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v(" GetAvailabilitieTask", "sending GetAvailabilitieTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getFlatsEndpoint() + "/" + String.valueOf(id) + "/availabilities";
        ResponseEntity<GetAvailabilitiesResponse> getAvailabilitieTaskEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetAvailabilitiesResponse.class);
        return getAvailabilitieTaskEntity;
    }

    public static GetAvailabilitiesResponse unpack(AsyncTask<Integer, Void, ResponseEntity<GetAvailabilitiesResponse>> getAvailabilitieTaskEntity)
    {
        try{
            ResponseEntity<GetAvailabilitiesResponse> responseEntity = getAvailabilitieTaskEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                GetAvailabilitiesResponse advertisementsByIdResponse  = responseEntity.getBody();
                if(advertisementsByIdResponse != null) {
                    return advertisementsByIdResponse;
                }
                else {
                    Log.v(" GetAvailabilitieTask", "GetAvailabilitieTask == null");
                    return null;
                }
            }
            else{
                Log.v(" GetAvailabilitieTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v(" GetAdvTaskByIdTask", e.toString());
        }
        return null;
    }
}
