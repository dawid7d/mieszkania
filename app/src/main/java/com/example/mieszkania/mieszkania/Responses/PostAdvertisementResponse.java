package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PostAdvertisementResponse {

    boolean success = false;
    Data data;

    public static class Data{

        public Advertisement getAdvertisement() {
            return advertisement;
        }

        Advertisement advertisement;

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Advertisement{
            Integer id = 0;
            Integer user_id = 0;
            String target_type = "";
            Integer target_id = 0;
            String title = "";
            Double rental_price = 0.0;
            Double deposit = 0.0;
            boolean is_active = false;
            String start_rent_date = "";
            String end_rent_date = "";
            String contract_type = "";
            boolean agency = false;
            Double agency_commission = 0.0;

            public String getTitle() {
                return title;
            }

            public String getTarget_type() {
                return target_type;
            }

            public String getStart_rent_date() {
                return start_rent_date;
            }

            public String getEnd_rent_date() {
                return end_rent_date;
            }

            public String getContract_type() {
                return contract_type;
            }

            public Integer getTarget_id() {
                return target_id;
            }

            public Double getRental_price() {
                return rental_price;
            }

            public Double getDeposit() {
                return deposit;
            }

            public Double getAgency_commission() {
                return agency_commission;
            }

            public Integer getId() {
                return id;
            }

            public Integer getUser_id() {
                return user_id;
            }

            public boolean isAgency() {
                return agency;
            }

            public boolean isActive() {
                return is_active;
            }
        }
    }
    String[] error;

    public boolean isSuccess() {
        return success;
    }

    public Data getData() {
        return data;
    }

    public String[] getError() {
        return error;
    }
}
