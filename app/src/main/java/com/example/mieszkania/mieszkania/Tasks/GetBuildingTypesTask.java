package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.GetBuildingTypesResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetBuildingTypesTask extends AsyncTask<Void, Void, ResponseEntity<GetBuildingTypesResponse>> {

    @Override
    protected ResponseEntity<GetBuildingTypesResponse> doInBackground(Void ... params) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetBuildingTypesTask", "sending GetBuildingTypesTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getBuildingTypesEndpoint();
        ResponseEntity<GetBuildingTypesResponse> getBuildingTypesResponseResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetBuildingTypesResponse.class);
        return getBuildingTypesResponseResponseEntity;
    }

    public static GetBuildingTypesResponse unpack(AsyncTask<Void, Void, ResponseEntity<GetBuildingTypesResponse>>  responseEntityAsyncTask)
    {
        try{
            ResponseEntity<GetBuildingTypesResponse> responseEntity = responseEntityAsyncTask.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                GetBuildingTypesResponse flatsResponse  = responseEntity.getBody();
                if(flatsResponse != null) {
                    return flatsResponse;
                }
                else {
                    Log.v("GetBuildingTypesTask", "flatsResponse == null");
                    return null;
                }
            }
            else{
                Log.v("GetBuildingTypesTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetBuildingTypesTask", e.toString());
        }
        return null;
    }
}
