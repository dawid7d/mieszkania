package com.example.mieszkania.mieszkania.Responses;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetWindowsTypesResponse {
    Boolean success;
    Data data;

    public static class Data {
        String[] windows_types;

        public String[] getWindows_types() {
            return windows_types;
        }
    }

    public Boolean getSuccess() {
        return success;
    }

    public Data getData() {
        return data;
    }
}
