package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.GetFlatAccessoriesResponse;
import com.example.mieszkania.mieszkania.Responses.GetFlatRoomsResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetFlatRoomsTask extends AsyncTask<Integer, Void, ResponseEntity<GetFlatRoomsResponse>> {

    @Override
    protected ResponseEntity<GetFlatRoomsResponse> doInBackground(Integer ... params) {
        Integer id = params[0];
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetFlatRoomsTask", "sending GetFlatRoomsTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getFlatsEndpoint() + "/" + String.valueOf(id) + "/" + "rooms";
        ResponseEntity<GetFlatRoomsResponse> GetFlatRoomsResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetFlatRoomsResponse.class);
        return GetFlatRoomsResponseEntity;
    }

    public static GetFlatRoomsResponse unpack(AsyncTask<Integer, Void, ResponseEntity<GetFlatRoomsResponse>> roomsResponseEntity)
    {
        try{
            ResponseEntity<GetFlatRoomsResponse> responseEntity = roomsResponseEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                GetFlatRoomsResponse roomsResponse = responseEntity.getBody();
                if(roomsResponse != null) {
                    return roomsResponse;
                }
                else {
                    Log.v("GetFlatRoomssTask", "GetRoomTypeAccessoriesResponese == null");
                    return null;
                }
            }
            else{
                Log.v("GetFlatRoomsTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetFlatRoomsTask", e.toString());
        }
        return null;
    }
}
