package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostFlatsResponse {
    boolean success;

    Data data;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data {

        public Flat getFlat() {
            return flat;
        }

        public void setFlat(Flat flat) {
            this.flat = flat;
        }

        Flat flat;

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Flat{
            private int id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }
        }

    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }
}
