package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Requests.GetAccessoriesRequest;
import com.example.mieszkania.mieszkania.Responses.AccessoryResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetAccessoriesTask extends AsyncTask<String, Void, ResponseEntity<AccessoryResponse>> {

    @Override
    protected ResponseEntity<AccessoryResponse> doInBackground(String ... params) {
        String chosenRoomType = params[0];

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetAccessoriesTask", "sending GetAccessoriesTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        GetAccessoriesRequest getAccessoriesRequest = new GetAccessoriesRequest();
        getAccessoriesRequest.setRoom_type(chosenRoomType);

        HttpEntity<GetAccessoriesRequest> request = new HttpEntity<>(getAccessoriesRequest, headers);
        String url = Config.getHost() + Config.getAccessoriesEndpoint();
        ResponseEntity<AccessoryResponse> accessoryResponseResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                request,
                AccessoryResponse.class);
        return accessoryResponseResponseEntity;
    }

    public static AccessoryResponse unpack(AsyncTask<String, Void, ResponseEntity<AccessoryResponse>> accessoryResponseResponseEntity)
    {
        try{
            ResponseEntity<AccessoryResponse> responseEntity = accessoryResponseResponseEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                AccessoryResponse accessoryResponse = responseEntity.getBody();
                if(accessoryResponse != null) {
                    return accessoryResponse;
                }
                else {
                    Log.v("GetAccessoriesTask", "GetRoomTypeAccessoriesResponese == null");
                    return null;
                }
            }
            else{
                Log.v("GetAccessoriesTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetAccessoriesTask", e.toString());
        }
        return null;
    }
}
