package com.example.mieszkania.mieszkania.Activities;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.mieszkania.mieszkania.Flat;
import com.example.mieszkania.mieszkania.GuestAdvertisement;
import com.example.mieszkania.mieszkania.R;

public class MainActivity extends AppCompatActivity {

    Button Login;
    Button Registration;
    Button Service;
    Button Remind_password;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Service = findViewById(R.id.service);
        Login = findViewById(R.id.login);
        Registration = findViewById(R.id.registration);
        Remind_password = findViewById(R.id.remind_password);

        Service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,GuestAdvertisement.class);
                startActivity(intent);
            }
        });

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });

        Registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

        Remind_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                View view1 = getLayoutInflater().inflate(R.layout.remind_pass_dialog,null);
                builder.setView(view1);
                final AlertDialog dialog = builder.create();
                dialog.show();

                Button send = view1.findViewById(R.id.send);
                send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO:
                        Toast.makeText(MainActivity.this,"Wysłano instrukcje na podany email", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
            }
        });
    }
}
