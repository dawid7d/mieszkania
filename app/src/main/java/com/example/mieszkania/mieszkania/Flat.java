package com.example.mieszkania.mieszkania;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.Requests.PostAdvertisementRequest;
import com.example.mieszkania.mieszkania.Responses.AdvertisementsResponse;
import com.example.mieszkania.mieszkania.Responses.DeleteFlatResponse;
import com.example.mieszkania.mieszkania.Responses.FlatByIdResponse;
import com.example.mieszkania.mieszkania.Responses.GetFlatAccessoriesResponse;
import com.example.mieszkania.mieszkania.Responses.GetFlatRoomsResponse;
import com.example.mieszkania.mieszkania.Responses.GetImageResponse;
import com.example.mieszkania.mieszkania.Responses.PostAdvertisementResponse;
import com.example.mieszkania.mieszkania.Tasks.DeleteFlatTask;
import com.example.mieszkania.mieszkania.Tasks.GetFlatAccessoriesTask;
import com.example.mieszkania.mieszkania.Tasks.GetFlatRoomsTask;
import com.example.mieszkania.mieszkania.Tasks.GetImageTask;
import com.example.mieszkania.mieszkania.Tasks.PostFlatsAccessoriesTask;
import com.example.mieszkania.mieszkania.Tasks.PostAdvertisementTask;
import com.example.mieszkania.mieszkania.User.UserAddRooms;
import com.example.mieszkania.mieszkania.User.UserState;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Flat extends AppCompatActivity {

    TextView advertisement_information, flat_information, equipment;
    Button flat_photos,all_rooms,availability,delete_flat,flat_edit,add_room,add_flat_advert;
    Integer[] img;
    ArrayList<String> images;

    Boolean is_loging = false,can_edit = false;

    FlatByIdResponse.Data.Flat flat;
    AdvertisementsResponse.Data.Advertisements advertisements;

    ArrayList<String> accessories_list;
    ArrayList<String> rooms_list;
    ArrayList<GetFlatRoomsResponse.Data.Rooms> rooms;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flat);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            is_loging = extras.getBoolean("is_login");
            can_edit = extras.getBoolean("can_edit");
            flat = (FlatByIdResponse.Data.Flat) getIntent().getSerializableExtra("Flat");
            if(getIntent().getSerializableExtra("Advertisement") != null){
                advertisements = (AdvertisementsResponse.Data.Advertisements) getIntent().getSerializableExtra("Advertisement");
            }
            Log.v("Flat id", String.valueOf(flat.getId()));
        }
        availability = findViewById(R.id.availability);

        flat_edit = findViewById(R.id.flat_edit);
        delete_flat = findViewById(R.id.delete_flat);
        add_room = findViewById(R.id.add_room);
        add_flat_advert = findViewById(R.id.add_flat_advert);
        if(can_edit == false){
            flat_edit.setVisibility(View.GONE);
            delete_flat.setVisibility(View.GONE);
            add_room.setVisibility(View.GONE);
            add_flat_advert.setVisibility(View.GONE);
        }

        accessories_list = new ArrayList<>();

        GetFlatAccessoriesResponse getFlatAccessoriesResponse = GetFlatAccessoriesTask.unpack(new GetFlatAccessoriesTask().execute(flat.getId()));
        Log.v("##", String.valueOf(Array.getLength(getFlatAccessoriesResponse.getData().getDescribed_accessories())));
        for(GetFlatAccessoriesResponse.Data.Described_accessories a :
                getFlatAccessoriesResponse.getData().getDescribed_accessories() ){
            Log.v("##","id=" + a.getId()+
                                " accessory_id=" + a.getAccessory_id()+
                                " +accessory description="+ a.getDescription()+
                                " +accessories_description_name="+a.getAccessory_description_name()+
                                " +accessories_name="+a.getAccessory_name());
            accessories_list.add(a.getAccessory_name()+":  "+a.getAccessory_description_name()+", "+a.getDescription()+"\n");
        }


        //TODO: Take all info from DB
        advertisement_information = findViewById(R.id.advertisement_information);
        if(getIntent().getSerializableExtra("Advertisement") != null) {
            advertisement_information.setText(
                    advertisements.getTitle() + "\n" +
                            "Czynsz miesięczny: " + advertisements.getRental_price() + "zł\n" +
                            "Zaliczka: " + advertisements.getDeposit() + "\n" +
                            "Od:" + advertisements.getStart_rent_data() + "\n" +
                            "Do:" + advertisements.getEnd_rent_data() + "\n" +
                            "Typ umowy:" + advertisements.getContract_type());
        }else {
            advertisement_information.setText("Mieszkanie");
        }

        flat_information = findViewById(R.id.flat_information);
        flat_information.setText(
                "Informacje dot. mieszkania: \n\n"+
                "Miasto: " + flat.getAddress_city() + "\n" +
                "Ulica: " + flat.getAddress_street() + "\n" +
                "Nr. budynku: " + flat.getAddress_building_number() + "\n" +
                "Nr. mieszkania: " + flat.getAddress_flat_number() + "\n" +
                "Piętro: " + flat.getFloor() +"\n" +
                "Typ zabudowy: " + flat.getBuilding_type() +"\n" +
                "Metraż: " + flat.getYardage() +" m^2\n" +
                "Rok budowy: " + flat.getBuilding_year() +"\n" +
                "Liczba pomieszczeń: " + flat.getRooms_number() +"\n" +
                "Typ okien: " + flat.getWindows_type() +"\n" +
                "Opłata za media: " + flat.getMedia_cost() +" zł\n" +
                "Opłaty dodatkowe: " + flat.getAdditional_rent_cost() +" zł\n");

        flat_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Flat.this);
                View view2 = getLayoutInflater().inflate(R.layout.flat_edit_dialog,null);
                builder.setView(view2);
                final AlertDialog dialog = builder.create();
                dialog.show();

                Button save = view2.findViewById(R.id.save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

        //TODO: Take flat image from DB
        img = new Integer[]{R.drawable.img1, R.drawable.img_5, R.drawable.img_6};


        images = new ArrayList<>();
        GetImageResponse getImageResponse = GetImageTask.unpack(new GetImageTask().execute(flat.getId()));
        for( GetImageResponse.Data.Photos p : getImageResponse.getData().getPhotos()){
            Log.v("##"," id=" +p.getId() + " url=" + p.getImage_url());
            images.add("https://flatservice-api.herokuapp.com/"+p.getImage_url());
        }

        flat_photos = findViewById(R.id.flat_photos);
        flat_photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Flat.this);
                View view1 = getLayoutInflater().inflate(R.layout.flat_photos_dialog,null);
                builder.setView(view1);
                final AlertDialog dialog = builder.create();
                dialog.show();

                CustomAdapter customAdapter = new CustomAdapter();
                ListView photo_listView = view1.findViewById(R.id.flat_all_photos);
                photo_listView.setAdapter(customAdapter);
            }
        });

        rooms_list = new ArrayList<>();
        rooms = new ArrayList<>();
        GetFlatRoomsResponse roomsResponse = GetFlatRoomsTask.unpack(new GetFlatRoomsTask().execute(flat.getId()));
        for(GetFlatRoomsResponse.Data.Rooms r : roomsResponse.getData().getRooms()){
            Log.v("ave","id="+r.getId()+
                                 " flat_id="+r.getFlat_id()+
                                 " room_type="+r.getRoom_type()+
                                 " yardage="+r.getYardage()+
                                 " total_places="+r.getTotal_places()+
                                 " avaibles_places="+r.getAvailable_places()+
                                 " can+_rent_separatly="+r.getCan_rent_separately());
            rooms.add(r);
            rooms_list.add(r.getRoom_type());
         }
        final ListAdapter room_listAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,rooms_list);

        all_rooms = findViewById(R.id.all_rooms);
        all_rooms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Flat.this);
                View view2 = getLayoutInflater().inflate(R.layout.all_rooms_dialog,null);
                builder.setView(view2);
                final AlertDialog dialog = builder.create();
                dialog.show();

                ListView room_listView = view2.findViewById(R.id.rooms_list);
                room_listView.setAdapter(room_listAdapter);

                room_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent intent = new Intent(Flat.this,Room.class);
                        UserState.setCanEdit(true);
                        intent.putExtra("can_edit", UserState.getCanEdit());
                        intent.putExtra("Room", rooms.get(i));
                        intent.putExtra("flat_id",flat.getId());
                        startActivity(intent);
                    }
                });
            }
        });

        add_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Flat.this,UserAddRooms.class);
                intent.putExtra("rooms_number","1");
                intent.putExtra("flat_id", String.valueOf(flat.getId()));
                startActivity(intent);
            }
        });

        equipment = findViewById(R.id.equipment);
        equipment.append("Akcesoria w mieszkaniu: \n\n");
        for( String a : accessories_list){
            equipment.append(a);
        }

        add_flat_advert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Flat.this);
                final View view2 = getLayoutInflater().inflate(R.layout.add_flat_advert,null);
                builder.setView(view2);
                final AlertDialog dialog = builder.create();
                dialog.show();

                final String[] arraySpinner = new String[] {"czas okreslony", "czas nieokreslony"};
                final Spinner contract_type_spinner =  view2.findViewById(R.id.contract_type_spinner);
                adapter = new ArrayAdapter<>(getApplicationContext(),
                        android.R.layout.simple_spinner_item, arraySpinner);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                contract_type_spinner.setAdapter(adapter);

                final CheckBox checkBox3 = view2.findViewById(R.id.checkBox3);

                final Button save = view2.findViewById(R.id.save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        EditText title_value = view2.findViewById(R.id.title_value);
                        EditText rental_price_value = view2.findViewById(R.id.rental_price_value);
                        EditText deposit_value = view2.findViewById(R.id.deposit_value);
                        EditText start_rent_date = view2.findViewById(R.id.start_value);
                        EditText end_rent_date = view2.findViewById(R.id.end_value);
                        EditText agency_commission = view2.findViewById(R.id.agency_commission_value);
                        boolean is_agency = checkBox3.isChecked();

                        Log.v("##"," target_type=" + "Flat\n" +
                                " target_id= " + flat.getId()+"\n"+
                                " title=" + title_value.getText()+"\n"+
                                " rental_price="+ rental_price_value.getText()+"\n"+
                                " deposit="+ deposit_value.getText()+"\n"+
                                " start_rent_date="+ start_rent_date.getText()+"\n"+
                                " end_rent_date="+ end_rent_date.getText()+"\n"+
                                " contract_type="+ contract_type_spinner.getSelectedItem()+"\n"+
                                " agency_is_checked="+ is_agency+"\n"+
                                " agency_commission="+ agency_commission.getText()+"\n"
                        );
                        PostAdvertisementRequest request = new PostAdvertisementRequest();
                        request.setTarget_type(PostAdvertisementRequest.targetFlat);
                        request.setTarget_id(flat.getId());
                        request.setTitle(title_value.getText().toString());
                        request.setRental_price(Double.valueOf(rental_price_value.getText().toString()));
                        request.setDeposit(Double.valueOf(deposit_value.getText().toString()));
                        request.setStart_rent_date(start_rent_date.getText().toString());
                        request.setEnd_rent_date(end_rent_date.getText().toString());
                        request.setContract_type(PostAdvertisementRequest.indefinite);

                        if(contract_type_spinner.getSelectedItem().toString().equals(arraySpinner[0])){
                            request.setContract_type(PostAdvertisementRequest.indefinite);
                        }else {
                            request.setContract_type(PostAdvertisementRequest.fixedTerm);
                        }

                        request.setAgency(is_agency);
                        if(!is_agency)
                            request.setAgency_commission(0.0);
                        else
                            request.setAgency_commission(Double.valueOf(agency_commission.getText().toString()));

                        PostAdvertisementResponse response = PostAdvertisementTask.unpack(new PostAdvertisementTask().execute(request));
                        Log.v("##","response="+response.isSuccess());
                        dialog.dismiss();
                    }
                });
            }
        });

        availability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Flat.this,Availabilities.class);
                intent.putExtra("can_edit", UserState.getCanEdit());
                intent.putExtra("flat_id", flat.getId());
                startActivity(intent);
            }
        });

        delete_flat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: use proper flat_id
                DeleteFlatResponse response = DeleteFlatTask.unpack(new DeleteFlatTask().execute(new Integer(0xabcde)));
                Log.v("DeleteFlat", String.valueOf(response.isSuccess()));

                for(String error : response.getError())
                    Log.v("DeleteFlat", error);
            }
        });
    }

    class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return img.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.photo, null);
            ImageView imageView = view.findViewById(R.id.flat_photo);
            imageView.setImageResource(img[i]);

//            String Url = images.get(i);
//            Drawable drawable = LoadImageFromWebOperations(Url);
//            imageView.setImageDrawable(drawable);

//            String Url = "https://flatservice-api.herokuapp.com/system/photos/images/000/000/023/original/file.jpg?1528819698";
//            Drawable drawable = LoadImageFromWebOperations(Url);
//            imageView.setImageDrawable(drawable);

            return view;
        }

        private Drawable LoadImageFromWebOperations(String url) {

            try {
                InputStream is = (InputStream) new URL(url).getContent();
                Drawable d = Drawable.createFromStream(is, "src name");
                return d;
            } catch (Exception e) {
                System.out.println("Exc=" + e);
                return null;
            }
        }
    }

}


