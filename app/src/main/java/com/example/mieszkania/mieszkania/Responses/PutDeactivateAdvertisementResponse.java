package com.example.mieszkania.mieszkania.Responses;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class PutDeactivateAdvertisementResponse {
    boolean success;
    String[] error;

    public boolean isSuccess() {
        return success;
    }

    public String[] getError() {
        return error;
    }
}
