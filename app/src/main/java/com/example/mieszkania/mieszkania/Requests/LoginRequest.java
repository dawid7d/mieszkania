package com.example.mieszkania.mieszkania.Requests;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginRequest {

    public class User {
        private String email;
        private String password;

        public String getEmail() {
            return email;
        }

        public String getPassword(){
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        @Override
        public String toString(){
            return "User{"+
                    "email=" + this.email + '\'' +
                    ", password=" + this.password +
                    '}';
        }
    }
    User user;

    public LoginRequest() {
        user = new User();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString(){
        return "LoginRequest{"+
                "user=" + this.user +
                '}';
    }
}
