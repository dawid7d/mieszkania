package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.AccessoryDescyriptionByIdResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class AccessoryDescriptionByIdTask extends AsyncTask<Integer, Void, ResponseEntity<AccessoryDescyriptionByIdResponse>> {

    @Override
    protected ResponseEntity<AccessoryDescyriptionByIdResponse> doInBackground(Integer...params) {
        Integer id = params[0];
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("AccessoryDescByIdTask", "sending AccessoryDescByIdResp with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getAccessoriesEndpoint() + "/" + String.valueOf(id) + "/accessory_descriptions";
        ResponseEntity<AccessoryDescyriptionByIdResponse> accessoryDescyriptionByIdResponseResponseEntity= restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                AccessoryDescyriptionByIdResponse.class);
        return accessoryDescyriptionByIdResponseResponseEntity;
    }

    public static AccessoryDescyriptionByIdResponse unpack(AsyncTask<Integer, Void, ResponseEntity<AccessoryDescyriptionByIdResponse>> accessoryDescyriptionByIdResponseeEntity)
    {
        try{
            ResponseEntity<AccessoryDescyriptionByIdResponse> responseEntity = accessoryDescyriptionByIdResponseeEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                AccessoryDescyriptionByIdResponse accessoryDescyriptionByIdResponse  = responseEntity.getBody();
                if(accessoryDescyriptionByIdResponse != null) {
                    return accessoryDescyriptionByIdResponse;
                }
                else {
                    Log.v("AccessoryDescByIdResp", " AccessoryDescyriptionByIdResponse == null");
                    return null;
                }
            }
            else{
                Log.v("AccessoryDescByIdResp", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("AccessoryDescByIdResp", e.toString());
        }
        return null;
    }
}
