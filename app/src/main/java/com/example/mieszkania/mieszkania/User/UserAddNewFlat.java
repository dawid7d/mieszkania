package com.example.mieszkania.mieszkania.User;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.mieszkania.mieszkania.R;
import com.example.mieszkania.mieszkania.Requests.PostFlatsRequest;
import com.example.mieszkania.mieszkania.Responses.GetBuildingTypesResponse;
import com.example.mieszkania.mieszkania.Responses.GetWindowsTypesResponse;
import com.example.mieszkania.mieszkania.Responses.PostFlatsResponse;
import com.example.mieszkania.mieszkania.Tasks.GetBuildingTypesTask;
import com.example.mieszkania.mieszkania.Tasks.GetWindowsTypesTask;
import com.example.mieszkania.mieszkania.Tasks.PostFlatsTask;

import java.util.ArrayList;

public class UserAddNewFlat extends AppCompatActivity {

    EditText rooms_value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_add_new_flat);

        rooms_value = findViewById(R.id.rooms_value);

        ArrayList<String> buildingType = new ArrayList<>();
        GetBuildingTypesResponse getBuildingTypesResponse
                = GetBuildingTypesTask.unpack(new GetBuildingTypesTask().execute());
        for (String type : getBuildingTypesResponse.getData().getBuilding_types()) {
            buildingType.add(type);
        }

        final Spinner building_spinner = findViewById(R.id.building_type_value);
        ArrayAdapter<String> building_spinner_adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, buildingType);
        building_spinner.setAdapter(building_spinner_adapter);


        ArrayList<String> windwosType = new ArrayList<>();
        GetWindowsTypesResponse getWindowsTypesResponse = GetWindowsTypesTask.unpack(new GetWindowsTypesTask().execute());
        for (String type : getWindowsTypesResponse.getData().getWindows_types()) {
            windwosType.add(type);
        }

        final Spinner windows_spinner = findViewById(R.id.window_type_value);
        ArrayAdapter<String> widnows_spinner_adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, windwosType);
        windows_spinner.setAdapter(widnows_spinner_adapter);


        Button add_flat = findViewById(R.id.send_flat);
        add_flat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int flat_id = saveFlat();
                Intent intent = new Intent(UserAddNewFlat.this, UserAddFlatAccessories.class);
                Bundle extras = new Bundle();
                extras.putString("rooms_number", String.valueOf(rooms_value.getText()));
                extras.putString("flat_id", String.valueOf(flat_id));
                intent.putExtras(extras);
                startActivity(intent);
            }
        });

    }
    int saveFlat(){
        PostFlatsRequest flatsRequest = new PostFlatsRequest();

        EditText additional_rent_cost_value =  findViewById(R.id.additional_rent_cost_value);
        EditText media_cost_value = findViewById(R.id.media_cost_value);
        EditText address_city_value = findViewById(R.id.address_city_value);
        EditText address_building_number_value = findViewById(R.id.address_building_number_value);
        EditText address_flat_number_value = findViewById(R.id.address_flat_number_value);
        EditText address_street_value = findViewById(R.id.address_street_value);
        EditText floor_value = findViewById(R.id.floor_value);
        EditText rooms_value = findViewById(R.id.rooms_value);
        EditText yardage_value = findViewById(R.id.yardage_value);
        EditText building_year_value = findViewById(R.id.building_year_value);
        Spinner building_type_value = findViewById(R.id.building_type_value);
        Spinner window_type_value = findViewById(R.id.window_type_value);


        flatsRequest.getFlat().setAdditional_rent_cost(Double.parseDouble(additional_rent_cost_value.getText().toString()));
        flatsRequest.getFlat().setMedia_cost(Double.parseDouble(media_cost_value.getText().toString()));
        flatsRequest.getFlat().setAddress_city(address_city_value.getText().toString());
        flatsRequest.getFlat().setAddress_building_number(address_building_number_value.getText().toString());
        flatsRequest.getFlat().setAddress_flat_number(address_flat_number_value.getText().toString());
        flatsRequest.getFlat().setAddress_street(address_street_value.getText().toString());
        flatsRequest.getFlat().setFloor(Integer.parseInt(floor_value.getText().toString()));
        flatsRequest.getFlat().setRooms_number(Integer.parseInt(rooms_value.getText().toString()));
        flatsRequest.getFlat().setYardage(Double.parseDouble(yardage_value.getText().toString()));
        flatsRequest.getFlat().setBuilding_year(Integer.parseInt(building_year_value.getText().toString()));
        flatsRequest.getFlat().setBuilding_type(building_type_value.getSelectedItem().toString());
        flatsRequest.getFlat().setWindows_type(window_type_value.getSelectedItem().toString());

        PostFlatsResponse postFlatsResponse = PostFlatsTask.unpack(new PostFlatsTask().execute(flatsRequest));

        Log.v("postFlatsResponse", "success = " + String.valueOf(postFlatsResponse.isSuccess()));
        Log.v("postFlatResponse", "id="+postFlatsResponse.getData().getFlat().getId());

        return postFlatsResponse.getData().getFlat().getId();
    }
}
