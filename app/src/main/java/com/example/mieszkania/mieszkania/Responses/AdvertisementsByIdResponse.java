package com.example.mieszkania.mieszkania.Responses;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AdvertisementsByIdResponse {

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    boolean success;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    Data data;

    public static class Data {

        public Advertisement getAdvertisement() {
            return Advertisement;
        }

        public void setAdvertisement( Advertisement Advertisement) {
            this.Advertisement = Advertisement;
        }

        Advertisement Advertisement;

//        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Advertisement{

            public int getId() {
                return id;
            }

            public int getUser_id() {
                return user_id;
            }

            public String getTarget_type() {
                return target_type;
            }

            public int getTarget_id() {
                return target_id;
            }

            public String getTitle() {
                return title;
            }

            public Double getRental_price() {
                return rental_price;
            }

            public Double getDeposit() {
                return deposit;
            }

            public boolean isIs_active() {
                return is_active;
            }

            public String getStart_rent_date() {
                return start_rent_date;
            }

            public String getEnd_rent_date() {
                return end_rent_date;
            }

            public String getContract_type() {
                return contract_type;
            }

            public boolean isAgency() {
                return agency;
            }

            public Double getAgency_commission() {
                return agency_commission;
            }

            public String getCreated_at() {
                return created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public String getAddress_gps() {
                return address_gps;
            }

            int id;
            int user_id;
            String target_type;
            int target_id;
            String title;
            Double rental_price;
            Double deposit;
            boolean is_active;
            String start_rent_date;
            String end_rent_date;
            String contract_type;
            boolean agency;
            Double agency_commission;
            String created_at;
            String updated_at;
            String address_gps;
        }
    }
}
