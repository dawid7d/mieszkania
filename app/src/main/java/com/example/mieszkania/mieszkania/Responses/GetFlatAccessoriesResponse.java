package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFlatAccessoriesResponse {
    boolean success;

    public static class Data {

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Described_accessories{
            private int id;
            private int accessory_id;
            private int accessory_description_id;
            private String accessory_description_name;
            private String accessory_name;
            private String description;

            public String getAccessory_description_name() {
                return accessory_description_name;
            }

            public void setAccessory_description_name(String accessory_description_name) {
                this.accessory_description_name = accessory_description_name;
            }

            public String getAccessory_name() {
                return accessory_name;
            }

            public void setAccessory_name(String accessory_name) {
                this.accessory_name = accessory_name;
            }

            public int getAccessory_id() {
                return accessory_id;
            }

            public void setAccessory_id(int accessory_id) {
                this.accessory_id = accessory_id;
            }

            public int getAccessory_description_id() {
                return accessory_description_id;
            }

            public void setAccessory_description_id(int accessory_description_id) {
                this.accessory_description_id = accessory_description_id;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

        }

        Described_accessories[] described_accessories;

        public Described_accessories[] getDescribed_accessories() {
            return described_accessories;
        }

        public void setDescribed_accessories(Described_accessories[] described_accessories) {
            this.described_accessories = described_accessories;
        }

    }

    Data data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
