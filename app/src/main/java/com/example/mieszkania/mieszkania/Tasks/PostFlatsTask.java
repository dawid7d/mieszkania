package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Requests.PostFlatsRequest;
import com.example.mieszkania.mieszkania.Responses.PostFlatsResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;


public class PostFlatsTask extends AsyncTask<PostFlatsRequest, Void, ResponseEntity<PostFlatsResponse>> {

    @Override
    protected ResponseEntity<PostFlatsResponse> doInBackground(PostFlatsRequest ... params) {
        PostFlatsRequest objToPass = params[0];
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("PostFlatsTask", "sending PostFlatsTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<PostFlatsRequest> request = new HttpEntity<>(objToPass, headers);
        String url = Config.getHost() + Config.getFlatsEndpoint();
        ResponseEntity<PostFlatsResponse> postFlatsResponseResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                request,
                PostFlatsResponse.class);
        return postFlatsResponseResponseEntity;
    }


    public static PostFlatsResponse unpack(AsyncTask<PostFlatsRequest, Void, ResponseEntity<PostFlatsResponse>>  postFlatsResponseResponseEntity)
    {
        try{
            ResponseEntity<PostFlatsResponse> responseEntity = postFlatsResponseResponseEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                PostFlatsResponse flatsResponse  = responseEntity.getBody();
                if(flatsResponse != null) {
                    return flatsResponse;
                }
                else {
                    Log.v("PostFlatsResponse", "PostFlatsResponse == null");
                    return null;
                }
            }
            else{
                Log.v("PostFlatsResponse", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("PostFlatsResponse", e.toString());
        }
        return null;
    }
}

