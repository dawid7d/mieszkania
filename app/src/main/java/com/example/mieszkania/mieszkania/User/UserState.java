package com.example.mieszkania.mieszkania.User;

public class UserState {
    private static Boolean isLogged;
    private static Boolean canEdit;
    private static String nextApiKey;

    private static String first_name;
    private static String last_name;
    private static int phone_number;
    private static String email;

    public static Boolean getIsLogged() {
        return isLogged;
    }

    public static void setIsLogged(Boolean isLogged) {
        UserState.isLogged = isLogged;
    }

    public static Boolean getCanEdit() {
        return canEdit;
    }

    public static void setCanEdit(Boolean canEdit) {
        UserState.canEdit = canEdit;
    }

    public static String getNextApiKey() {
        return nextApiKey;
    }

    public static void setNextApiKey(String nextApiKey) {
        UserState.nextApiKey = nextApiKey;
    }

    public static void setFirst_name(String first_name) {
        UserState.first_name = first_name;
    }

    public static String getFirst_name() {
        return first_name;
    }

    public static void setLast_name(String last_name) {
        UserState.last_name = last_name;
    }

    public static String getLast_name() {
        return last_name;
    }

    public static void setPhone_number(int phone_number) {
        UserState.phone_number = phone_number;
    }

    public static int getPhone_number() {
        return phone_number;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        UserState.email = email;
    }

}
