package com.example.mieszkania.mieszkania.Responses;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AdvertisementsResponse {

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    boolean success;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    Data data;

    public static class Data {

        public Advertisements[] getAdvertisement() {
            return Advertisement;
        }

        public void setAdvertisements(Advertisements[] Advertisement) {
            this.Advertisement = Advertisement;
        }

        Advertisements[] Advertisement;

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Advertisements implements Serializable {

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public String getTarget_type() {
                return target_type;
            }

            public void setTarget_type(String target_type) {
                this.target_type = target_type;
            }

            public int getTarget_id() {
                return target_id;
            }

            public void setTarget_id(int target_id) {
                this.target_id = target_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public Double getRental_price() {
                return rental_price;
            }

            public void setRental_price(Double rental_price) {
                this.rental_price = rental_price;
            }

            public Double getDeposit() {
                return deposit;
            }

            public void setDeposit(Double deposit) {
                this.deposit = deposit;
            }

            public boolean isIs_active() {
                return is_active;
            }

            public void setIs_active(boolean is_active) {
                this.is_active = is_active;
            }

            public String getStart_rent_data() {
                return start_rent_date;
            }

            public void setStart_rent_data(String start_rent_data) {
                this.start_rent_date = start_rent_data;
            }

            public String getEnd_rent_data() {
                return end_rent_date;
            }

            public void setEnd_rent_data(String end_rent_data) {
                this.end_rent_date = end_rent_data;
            }

            public String getContract_type() {
                return contract_type;
            }

            public void setContract_type(String contract_type) {
                this.contract_type = contract_type;
            }

            public boolean isAgency() {
                return agency;
            }

            public void setAgency(boolean agency) {
                this.agency = agency;
            }

            public Double getAgency_commission() {
                return agency_commission;
            }

            public void setAgency_commission(Double agency_commission) {
                this.agency_commission = agency_commission;
            }

            int id;
            int user_id;
            String target_type;
            int target_id;
            String title;
            Double rental_price;
            Double deposit;
            boolean is_active;
            String start_rent_date;
            String end_rent_date;
            String contract_type;
            boolean agency;
            Double agency_commission;
            String created_at;
            String updated_at;
        }
    }
}
