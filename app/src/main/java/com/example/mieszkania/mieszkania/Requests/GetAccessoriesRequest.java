package com.example.mieszkania.mieszkania.Requests;



public class GetAccessoriesRequest {

    public GetAccessoriesRequest(){

    }

    String room_type;

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public static String FLAT = "flat";
    public static String ONE_PERSON = "one_person";
}
