package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetBuildingTypesResponse {
    Boolean success;
    Data data;

    public static class Data {
        String[] building_types;

        public String[] getBuilding_types() {
            return building_types;
        }
    }

    public Boolean getSuccess() {
        return success;
    }

    public Data getData() {
        return data;
    }
}

