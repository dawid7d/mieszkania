package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.GetPreferencesResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetPreferencesTask extends AsyncTask<Void, Void, ResponseEntity<GetPreferencesResponse>> {

    @Override
    protected ResponseEntity<GetPreferencesResponse> doInBackground(Void... params) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetPreferencesTask", "sending GetPreferencesReq with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getPreferencesEndpoint();
        ResponseEntity<GetPreferencesResponse> getPreferencesResponseResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                GetPreferencesResponse.class);
        return getPreferencesResponseResponseEntity;
    }

    public static GetPreferencesResponse unpack(AsyncTask<Void, Void, ResponseEntity<GetPreferencesResponse>>  getPreferencesResponseResponseEntity)
    {
        try{
            ResponseEntity<GetPreferencesResponse> responseEntity = getPreferencesResponseResponseEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                GetPreferencesResponse getPreferencesResponse  = responseEntity.getBody();
                if(getPreferencesResponse != null) {
                    return getPreferencesResponse;
                }
                else {
                    Log.v("GetPreferencesTask", "GetPreferencesResponse == null");
                    return null;
                }
            }
            else{
                Log.v("GetPreferencesTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetPreferencesTask", e.toString());
        }
        return null;
    }
}
