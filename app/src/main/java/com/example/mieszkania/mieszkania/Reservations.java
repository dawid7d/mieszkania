package com.example.mieszkania.mieszkania;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.Responses.AdvertisementsByIdResponse;
import com.example.mieszkania.mieszkania.Responses.GetUserReservationsResponse;
import com.example.mieszkania.mieszkania.Tasks.AdvertisementsTaskByIdTask;
import com.example.mieszkania.mieszkania.Tasks.GetUserReservationsTask;
import com.example.mieszkania.mieszkania.User.UserActiveAdvertisements;
import com.fasterxml.jackson.databind.ser.SerializerCache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Reservations extends AppCompatActivity {

    Map<String,Integer> advertisements_map;
    ArrayList<String> reservations;
    Map<Integer,AdvertisementsByIdResponse.Data.Advertisement> advertisements;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserwations);

        GetUserReservationsResponse getUserReservationsResponse =
                GetUserReservationsTask.unpack(new GetUserReservationsTask().execute());

        reservations = new ArrayList<>();
        advertisements_map = new HashMap<>();
        advertisements = new HashMap<>();

        for( GetUserReservationsResponse.Data.Reservations r : getUserReservationsResponse.getData().getReservations()){
            Log.v("!#","GET: "+r.getUser_id()+" "+r.getAdvertisement_id()+" "+r.getStart_date()+"\n");

            String s = " - Data: "+r.getStart_date().split("T")[0]+" "+r.getStart_date().split("T")[1].substring(0,5);
            advertisements_map.put(s,r.getAdvertisement_id());
            reservations.add(s);

            AdvertisementsByIdResponse advertisementsByIdResponse =
                    AdvertisementsTaskByIdTask.unpack(new AdvertisementsTaskByIdTask().execute(new Integer(r.getAdvertisement_id())));
            advertisements.put(r.getAdvertisement_id(),advertisementsByIdResponse.getData().getAdvertisement());
        }

          // don't delete this yet
//        for(int i=10; i<14; i++){
//            AdvertisementsByIdResponse advertisementsByIdResponse =
//                    AdvertisementsTaskByIdTask.unpack(new AdvertisementsTaskByIdTask().execute(i));
//            advertisements.put(i,advertisementsByIdResponse.getData().getAdvertisement());
//        }


        for(String r : reservations){
            Log.v("!#","reservation="+r);
        }

        for(Integer i : advertisements.keySet()){
            Log.v("!#","advertisements id="+ i + " "+ advertisements.get(i).getStart_rent_date());
        }

        for(String s : advertisements_map.keySet()){
                Log.v("!#",s + " -> "+advertisements_map.get(s));
        }

        AdvertisementsAdapter advertisementsAdapter = new AdvertisementsAdapter(reservations,advertisements,advertisements_map);
        listView = findViewById(R.id.advertisements_list);
        listView.setAdapter(advertisementsAdapter);
    }


    public class AdvertisementsAdapter extends BaseAdapter {

        ArrayList<String> reservations;
        Map<Integer,AdvertisementsByIdResponse.Data.Advertisement> advertisements;
        Map<String,Integer> advertisements_map;
        ArrayList<Integer> keys;

        AdvertisementsAdapter(ArrayList<String> reservations, Map<Integer,AdvertisementsByIdResponse.Data.Advertisement> advertisements, Map<String,Integer> advertisements_map){
            this.reservations = reservations;
            this.advertisements = advertisements;
            this.advertisements_map = advertisements_map;
            this.keys = new ArrayList(advertisements.keySet());
        }

        @Override
        public int getCount() {
            return advertisements.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.flat_adapter_layout,null);

            TextView textView = view.findViewById(R.id.textView);

            int id = keys.get(i);
            Log.v("!#"," id_ogłoszenia="+id);

            String ad_active = " Aktywne ";
                if(advertisements.get(id).isIs_active()) {ad_active="Tak";}
                else {ad_active="Nie";}

            textView.append(advertisements.get(id).getTitle()+ "\n\n"+
                    " Koszt miesieczny: " + advertisements.get(id).getRental_price()+ "\n"+
                    " Zaliczka: " + advertisements.get(id).getDeposit()+ "\n"+
                    " Czy aktywne:" + ad_active+ "\n"+
                    " Koszt miesieczny: " + advertisements.get(id).getRental_price()+ "\n"+
                    " Od: " + advertisements.get(id).getStart_rent_date()+"\n"+
                    " Do: " + advertisements.get(id).getEnd_rent_date()+"\n"
            );

            textView.append("\n\n Obecnie aktywne rezerwacje dla tego ogłoszenia: \n\n");

            for(String s : advertisements_map.keySet() ){

                if( advertisements_map.get(s) == id){
                    Log.v("!#","advertisements_map.get(s)="+advertisements_map.get(s)+" id="+id);
                    textView.append(s+"\n");
                }
            }

            return view;
        }
    }

}
