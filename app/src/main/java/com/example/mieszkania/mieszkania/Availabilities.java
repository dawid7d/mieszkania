package com.example.mieszkania.mieszkania;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.Responses.GetAvailabilitiesResponse;
import com.example.mieszkania.mieszkania.Tasks.GetAvailabilitieTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Availabilities extends AppCompatActivity {

    ArrayList<Integer> availabilities_id;
    ArrayList<String> day;
    ArrayList<String> start_time;
    ArrayList<String> end_time;

    ListView listView;
    Button add_availabilities;
    Boolean can_edit = false;
    TextView _day;
    Integer flat_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_availabilities);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            can_edit = extras.getBoolean("can_edit");
            flat_id = extras.getInt("flat_id");
        }
        add_availabilities = findViewById(R.id.add_availabiliti);

        if(can_edit == false) {
            add_availabilities.setVisibility(View.GONE);
        }

        availabilities_id = new ArrayList<>();
        day = new ArrayList<>();
        start_time = new ArrayList<>();
        end_time = new ArrayList<>();

        GetAvailabilitiesResponse availabilitiesResponse = GetAvailabilitieTask.unpack(new GetAvailabilitieTask().execute(flat_id));
        for( GetAvailabilitiesResponse.Data.Availabilities a : availabilitiesResponse.getData().getAvailabilities() ){
            availabilities_id.add(a.getId());
            day.add(a.getDay());
            start_time.add(a.getStart_time());
            end_time.add(a.getEnd_time());
            Log.v("##", "id="+a.getId()+"\n day="+a.getDay()+"\n start="+a.getStart_time()+"\n end"+a.getEnd_time());
        }

        final CustomAdapter customAdapter = new CustomAdapter();
        listView = findViewById(R.id.availabilities_list);
        listView.setAdapter(customAdapter);

        add_availabilities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Availabilities.this);
                final View view1 = getLayoutInflater().inflate(R.layout.add_availabiliti_dialog,null);
                builder.setView(view1);
                final AlertDialog dialog = builder.create();
                dialog.show();

                Button save = view1.findViewById(R.id.save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO: sent new availabiliti to DB

                        availabilities_id.add(availabilities_id.size());

                        Spinner spinner_day = view1.findViewById(R.id.chose_day);
                        String choosen_day = spinner_day.getSelectedItem().toString();
                        day.add(choosen_day);

                        Spinner spinner_start = view1.findViewById(R.id.chose_start);
                        String choosen_start_time = spinner_start.getSelectedItem().toString();
                        start_time.add(choosen_start_time);

                        Spinner spinner_end = view1.findViewById(R.id.chose_end);
                        String choosen_end_time = spinner_end.getSelectedItem().toString();
                        end_time.add(choosen_end_time);


                        listView.invalidateViews();
                        dialog.dismiss();
                    }
                });

                Button cancel = view1.findViewById(R.id.cancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return availabilities_id.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.availabilities_list,null);

            TextView _day = view.findViewById(R.id.day);
            _day.setText("Dzień:  "+day.get(i));

            TextView _stat_time = view.findViewById(R.id.time);
            _stat_time.setText("Od: "+start_time.get(i) + "    Do: "+end_time.get(i));

            Button _delete = view.findViewById(R.id.delete);
            Button _edit = view.findViewById(R.id.edit);
            Button _reseravation = view.findViewById(R.id.reservation);

            if(can_edit == false){
                _edit.setVisibility(View.GONE);
                _delete.setVisibility(View.GONE);
            } else {
                _reseravation.setVisibility(View.GONE);
            }

            _reseravation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Availabilities.this);
                    final View view1 = getLayoutInflater().inflate(R.layout.reservation_dialog,null);
                    builder.setView(view1);
                    final AlertDialog dialog = builder.create();
                    dialog.show();

                    int dayOfTheWeek = 1;
                    if(day.get(i).equals("sunday")){dayOfTheWeek=1;}
                    if(day.get(i).equals("monday")){dayOfTheWeek=2;}
                    if(day.get(i).equals("thusday")){dayOfTheWeek=3;}
                    if(day.get(i).equals("wednesday")){dayOfTheWeek=4;}
                    if(day.get(i).equals("thursday")){dayOfTheWeek=5;}
                    if(day.get(i).equals("friday")){dayOfTheWeek=6;}
                    if(day.get(i).equals("saturday")){dayOfTheWeek=7;}

                    Calendar date1 = Calendar.getInstance();
                    date1.add(Calendar.DATE,1);
                    while (date1.get(Calendar.DAY_OF_WEEK) != dayOfTheWeek) {
                        date1.add(Calendar.DATE, 1);
                    }

                    SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd");
                    String formated_date = format.format(date1.getTime());
                    Log.v("ave", "formated data = " + formated_date);

                    TextView reservationDay = view1.findViewById(R.id.reservation_day);
                    reservationDay.setText("\nData dostępnej rezerwacji:\n\n"+formated_date+"\n\nDostępne godziny:\n");

                    //TODO: get free reservations list and add to avaible_reaservation list


                    ArrayList<String> available_reserations = new ArrayList<>();
                    available_reserations.add("10:00");
                    available_reserations.add("11:00");
                    available_reserations.add("11:10");
                    available_reserations.add("12:15");
                    available_reserations.add("12:30");
                    available_reserations.add("12:30");
                    available_reserations.add("12:45");
                    available_reserations.add("13:00");

                    final Spinner reservation_spinner = view1.findViewById(R.id.reservation_spinner);
                    ArrayAdapter<String> widnows_spinner_adapter = new ArrayAdapter<>(getApplicationContext(),
                            android.R.layout.simple_spinner_dropdown_item, available_reserations);
                    reservation_spinner.setAdapter(widnows_spinner_adapter);

                    Button save_reservation = view1.findViewById(R.id.save_reservation);
                    save_reservation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //TODO: add reservation to DB
//                            Log.v("ave","user choosed time="+reservation_spinner.getSelectedItem().toString());
                            dialog.dismiss();
                        }
                    });
                }
            });

            _edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Availabilities.this);
                    final View view2 = getLayoutInflater().inflate(R.layout.availabilities_edit_dialog,null);
                    builder.setView(view2);
                    final AlertDialog dialog = builder.create();
                    dialog.show();

                    Button save = view2.findViewById(R.id.save);
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //TODO: edit (PUT) availabiliti in DB

                            Spinner spinner = view2.findViewById(R.id.chose_day);
                            String choosen_day = spinner.getSelectedItem().toString();
                            day.set(i,choosen_day);

                            EditText editText1 = view2.findViewById(R.id.time);
                            String choosen_start_time = editText1.getText().toString();
                            start_time.set(i,choosen_start_time);

                            EditText editText2 = view2.findViewById(R.id.end_time);
                            String choosen_end_time = editText2.getText().toString();
                            end_time.set(i,choosen_end_time);

                            listView.invalidateViews();
                            dialog.dismiss();
                        }
                    });

                    Button cancel = view2.findViewById(R.id.cancel);
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                }
            });

            _delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //TODO: delete availabiliti from DB
                    availabilities_id.remove(i);
                    day.remove(i);
                    start_time.remove(i);
                    end_time.remove(i);
                    listView.invalidateViews();
                }
            });

            return view;
        }
    }
}
