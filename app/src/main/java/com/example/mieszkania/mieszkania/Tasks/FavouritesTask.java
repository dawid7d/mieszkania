package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.FavouritesResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class FavouritesTask extends AsyncTask<Void, Void, ResponseEntity<FavouritesResponse>> {

    @Override
    protected ResponseEntity<FavouritesResponse> doInBackground(Void... voids) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetFavouritesTask", "sending GetFavouritesTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getFavourites();
        ResponseEntity<FavouritesResponse> getFavouritesResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                FavouritesResponse.class);
        return getFavouritesResponseEntity;
    }

    public static FavouritesResponse unpack(AsyncTask<Void, Void, ResponseEntity<FavouritesResponse>> getFavouritesResponseEntity)
    {
        try{
            ResponseEntity<FavouritesResponse> responseEntity = getFavouritesResponseEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                FavouritesResponse favouritesResponse  = responseEntity.getBody();
                if(favouritesResponse != null) {
                    return favouritesResponse;
                }
                else {
                    Log.v("GetFavouritesTask", "FavouritesResponse == null");
                    return null;
                }
            }
            else{
                Log.v("GetFavouritesTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetFavouritesTask", e.toString());
        }
        return null;
    }
}
