package com.example.mieszkania.mieszkania.User;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mieszkania.mieszkania.R;

public class UserData extends AppCompatActivity {

    TextView name;
    TextView last_name;
    TextView email;
    TextView phone;

    EditText _new_name;
    EditText _new_last_name;
    EditText _new_email;
    EditText _new_phone;
    EditText _current_password;
    EditText _password;
    EditText _conf_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);

        name = findViewById(R.id.name);
        last_name = findViewById(R.id.last_name);
        phone = findViewById(R.id.phone);
        email = findViewById(R.id.email);

        name.setText(UserState.getFirst_name());
        last_name.setText(UserState.getLast_name());
        email.setText(UserState.getEmail());
        phone.setText(Integer.toString(UserState.getPhone_number()));

        Button editData = findViewById(R.id.edit_data);
        editData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(UserData.this);
                View view1 = getLayoutInflater().inflate(R.layout.user_edit_data,null);
                builder.setView(view1);
                final AlertDialog dialog = builder.create();
                dialog.show();

                _new_name = view1.findViewById(R.id._new_name);
                _new_last_name = view1.findViewById(R.id._new_last_name);
                _new_phone = view1.findViewById(R.id._new_phone);

                Button save = view1.findViewById(R.id._save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO: upgrade data in DB
                        Log.v("ave","name = " + _new_name.getText());
                        Log.v("ave","_last_name = " + _new_last_name.getText());
                        Log.v("ave","_phone = " + _new_phone.getText());
                        dialog.dismiss();
                    }
                });
            }
        });

        Button editEmail = findViewById(R.id.edit_email);
        editEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(UserData.this);
                View view2 = getLayoutInflater().inflate(R.layout.user_edit_email,null);
                builder.setView(view2);
                final AlertDialog dialog = builder.create();
                dialog.show();

                _new_email = view2.findViewById(R.id._new_email);

                Button save = view2.findViewById(R.id._save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO: upgrade data in DB
                        Log.v("ave","email = " + _new_email.getText());
                        dialog.dismiss();
                    }
                });
            }
        });

        Button editPass = findViewById(R.id.edit_pass);
        editPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(UserData.this);
                View view3 = getLayoutInflater().inflate(R.layout.user_edit_pass,null);
                builder.setView(view3);
                final AlertDialog dialog = builder.create();
                dialog.show();

                _current_password = view3.findViewById(R.id._current_password);
                _password = view3.findViewById(R.id._password);
                _conf_password = view3.findViewById(R.id._confirm_password);

                Button save = view3.findViewById(R.id._save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO: upgrade data in DB
                        Log.v("ave","curr_pass = " + _current_password.getText());
                        Log.v("ave","pass = " + _password.getText());
                        Log.v("ave","conf_pass = " + _conf_password.getText());
                        dialog.dismiss();
                    }
                });
            }
        });

    }
}
