package com.example.mieszkania.mieszkania.Responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonalDataResponse {
    Boolean success;
    Data data;
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Data {
        @JsonIgnoreProperties(ignoreUnknown = true)
        public class User {
            String first_name;
            String last_name;
            int phone_number;

            public String getFirst_name() {
                return first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public int getPhone_number() {
                return phone_number;
            }
        }

        public User getUser() {
            return user;
        }
        User user;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Data getData() {
        return data;
    }
}
