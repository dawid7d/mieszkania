package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.PersonalDataResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


public class PersonalDataTask extends AsyncTask<Void, Void, ResponseEntity<PersonalDataResponse>> {

    @Override
    protected ResponseEntity<PersonalDataResponse> doInBackground(Void... voids) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());


        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());

        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getPersonalDataEndpoint();
        ResponseEntity<PersonalDataResponse> personalDataResponseResponseEntity =
                restTemplate.exchange(url, HttpMethod.GET, request, PersonalDataResponse.class);
        return personalDataResponseResponseEntity;
    }
}
