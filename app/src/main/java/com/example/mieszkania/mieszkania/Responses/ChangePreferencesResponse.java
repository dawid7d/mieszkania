package com.example.mieszkania.mieszkania.Responses;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangePreferencesResponse {
    Boolean success;
    Data data;

    public static class Data {
        public static class Customer{
            String place;
            Double distance;
            Double city_travel_time;
            Double car_travel_time;

            public Double getCar_travel_time() {
                return car_travel_time;
            }

            public Double getCity_travel_time() {
                return city_travel_time;
            }

            public Double getDistance() {
                return distance;
            }

            public String getPlace() {
                return place;
            }
        }
        Customer customer;

        public Customer getCustomer() {
            return customer;
        }
    }

    public Boolean getSuccess() {
        return success;
    }

    public Data getData() {
        return data;
    }
}