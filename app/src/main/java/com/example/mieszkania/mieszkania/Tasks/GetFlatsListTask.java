package com.example.mieszkania.mieszkania.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.mieszkania.mieszkania.Config;
import com.example.mieszkania.mieszkania.Responses.FlatsResponse;
import com.example.mieszkania.mieszkania.User.UserState;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class GetFlatsListTask extends AsyncTask<Void, Void, ResponseEntity<FlatsResponse>> {

    @Override
    protected ResponseEntity<FlatsResponse> doInBackground(Void... params) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("api-key", UserState.getNextApiKey());
        Log.v("GetFlatsListTask", "sending GetFlatsListTask with api-key" + UserState.getNextApiKey());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        String url = Config.getHost() + Config.getFlatsEndpoint();
        ResponseEntity<FlatsResponse> flatsResponseResponseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                FlatsResponse.class);
        return flatsResponseResponseEntity;
    }

    public static FlatsResponse unpack(AsyncTask<Void, Void, ResponseEntity<FlatsResponse>>  flatsResponseResponseEntity)
    {
        try{
            ResponseEntity<FlatsResponse> responseEntity = flatsResponseResponseEntity.get();
            List<String> list = responseEntity.getHeaders().get("api-key");
            if(list != null && list.size()>0){
                UserState.setNextApiKey(list.get(0));
                FlatsResponse flatsResponse  = responseEntity.getBody();
                if(flatsResponse != null) {
                    return flatsResponse;
                }
                else {
                    Log.v("GetFlatsListTask", "flatsResponse == null");
                    return null;
                }
            }
            else{
                Log.v("GetFlatsListTask", "no api-key in headers");
            }
        }
        catch (Exception e){
            Log.v("GetFlatsListTask", e.toString());
        }
        return null;
    }
}
