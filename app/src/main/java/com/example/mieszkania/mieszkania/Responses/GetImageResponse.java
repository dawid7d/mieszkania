package com.example.mieszkania.mieszkania.Responses;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class GetImageResponse {
    boolean success;

    Data data;

    public static class Data {

        Photos[] photos;

        public static class Photos {

            private int id;
            private String image_url;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImage_url() {
                return image_url;
            }

            public void setImage_url(String image_url) {
                this.image_url = image_url;
            }
        }

        public Photos[] getPhotos() {
            return photos;
        }
    }

    public boolean isSuccess() {
        return success;
    }

    public Data getData() {
        return data;
    }

}
