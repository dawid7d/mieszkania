package com.example.mieszkania.mieszkania;

import com.example.mieszkania.mieszkania.Requests.LoginRequest;
import com.example.mieszkania.mieszkania.Responses.LoginResponse;

import org.junit.Test;

import static org.junit.Assert.*;

import org.springframework.web.client.RestTemplate;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class LoginTest {

    @Test
    public void communication_with_mocked_backend_works() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:5000/users/sign_in";
        LoginRequest loginRequest = new LoginRequest();
        User user = new User();
        user.setEmail("userEmail");
        user.setPassword("userPassword");
        loginRequest.setUser(user);

        LoginResponse resp =  restTemplate.postForObject(url, loginRequest, LoginResponse.class);
        assertTrue(resp.isSuccess());
    }
}