package com.example.mieszkania.mieszkania;

import com.example.mieszkania.mieszkania.Requests.RegisterRequest;
import com.example.mieszkania.mieszkania.Responses.RegisterResponse;

import org.junit.Test;

import static org.junit.Assert.*;

import org.springframework.web.client.RestTemplate;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class RegisterTest {
    @Test
    public void communication_with_mocked_backend_works() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:5000/users/sign_up";
        RegisterRequest req = new RegisterRequest();
        User user = new User();
        user.setEmail("userEmail");
        user.setPassword("userPassword");
        req.setUser(user);
        req.setPassword_confirmation("userPassword");

        RegisterResponse resp =  restTemplate.postForObject(url, req, RegisterResponse.class);
        assertTrue(resp.isSuccess());
    }
}