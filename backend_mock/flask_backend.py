import flask
import json
from copy import deepcopy
from random import randint

def AssertEq(actual, expected):
    if not str(actual) == str(expected):
        print(repr(actual), ' != ', repr(expected))

app = flask.Flask(__name__)

mimetype_json = u'application/json'

login_response = {
    'success': True,
    'data':[],
    'error': 'noErrors',
}

register_response = {
    'success': True,
    'data':[],
    'error': 'noErrors',
}

personal_data_response = {
    'success': True,
    'data': {
         'user':{
             'first_name': "Zbyszek",
             'last_name': "Zbogdanca",
             'phone_number': 600123456
         }
    },
    'error':{}
}

flats_response = {
    'success': True,
    'data': {
         "flats": [
            {"id": 1},
            {"id": 2},
            {"id": 3},
            {"id": 4}
         ]
    },
    'error':{}
}

building_type_response = \
{
    'success': True,
    'data': {
         "building_types":[
             "tenement",
             "block",
             "residential",
             "house"
         ]
    },
    'error':["asdasd"]
}

windows_types_response = \
{
    'success': True,
    'data': {
         "windows_types":[
            "wooden",
            "plastic"
         ]
    },
    'error': {}
}

flat_by_id = {
    "success": True,
    "data": {
        "flat": {
            "id": 1,
            "additional_rent_cost": 59.99,
            "address_building_number": "13",
            "address_city": "Wrocław",
            "address_flat_number": "3A",
            "address_street": "Śliczna",
            "building_type": "kamienica",
            "building_year": 1998,
            "floor": 2,
            "media_cost": 450.0,
            "rooms_number": 4,
            "windows_type": "plastic",
            "yardage": 42.68
        } }
    ,
    "error":{}
}

preferencesResp = {
    "success": True,
    "data":  {
            "customer": {
                "place": "someplace",
                "distance": 5.0,
                "city_travel_time": 15.0,
                "car_travel_time": 5.0
            }
    },
    "error": {}
}

flats_post_response = {
    "success": True,
    "data":
        { "flat":
              { "id": 1,
                "additional_rent_cost": 59.99,
                "address_building_number": "13",
                "address_city": "Wrocław",
                "address_flat_number": "3A",
                "address_street": "Śliczna",
                "building_type": "tenement",
                "building_year": 1998,
                "floor": 2,
                "media_cost": 450.0,
                "rooms_number": 4,
                "windows_type": "plastic",
                "yardage": 42.68
            }
      },
    "error": []
}

flatAccessoriesResp = {
    "success": True,
    "data": {
            "accessories":[
                {"id":1,
                 "name":"kuchenka",
                 "room_type":"flat"},
                {"id":2,
                 "name":"odkurzacz",
                 "room_type":"flat"},
                {"id": 3,
                 "name": "telewizor",
                 "room_type": "flat"},
                {"id": 4,
                 "name": "piekarnik",
                 "room_type": "flat"},
                {"id": 5,
                 "name": "balkon",
                 "room_type": "flat"},
            ]
    },
    'error':{}
}

accessories_by_room_type = {
    "flat":[
        {"id":1,
         "name":"kuchenka",
         "room_type":"flat"},
        {"id":2,
         "name":"odkurzacz",
         "room_type":"flat"},
        {"id": 3,
         "name": "telewizor",
         "room_type": "flat"},
        {"id": 4,
         "name": "piekarnik",
         "room_type": "flat"},
        {"id": 5,
         "name": "balkon",
         "room_type": "flat"},
    ],
    "one_person": [
        {"id": 6,
         "name": "lozko",
         "room_type": "one_person"},
        {"id": 7,
         "name": "biurko",
         "room_type": "one_person"},
        {"id": 8,
         "name": "szafa",
         "room_type": "one_person"},
        {"id": 9,
         "name": "krzeslo",
         "room_type": "one_person"},
        {"id": 10,
         "name": "komoda",
         "room_type": "one_person"},
    ],

    "living_room": [
        {"id": 11,
         "name": "lozko",
         "room_type": "one_person"},
        {"id": 12,
         "name": "biurko",
         "room_type": "one_person"},
        {"id": 13,
         "name": "szafa",
         "room_type": "one_person"},
        {"id": 14,
         "name": "krzeslo",
         "room_type": "one_person"},
        {"id": 15,
         "name": "komoda",
         "room_type": "one_person"},
    ]
    
}

accessoryDescriptionsResp = {
    "success": True,
        "data": {
                "accessory_descriptions":[
                    {"id":1,
                     "name":"elektryczny"},
                    {"id":2,
                     "name":"mechaniczny"},
                    {"id": 3,
                     "name": "hydrauliczny"},
                ]
        },
        'error':{}
}

room_type_response = {
    "success": True,
    "data":
        { "room_types":
            [
                "one_person",
                "living_room",
                "flat"
            ]
        },
    "error": []
}


def generateFlatById(id):
    newFlat = deepcopy(flat_by_id)
    newFlat["data"]["flat"]["id"] = id
    return newFlat

class api_key_provider:
    api_key = 1
    
    @staticmethod
    def generateNext():
        api_key_provider.api_key+=1
        return api_key_provider.api_key
    
    @staticmethod
    def getLast():
        return api_key_provider.api_key
        
def validate_api_key(api_key):
    print(api_key)
    AssertEq(api_key,api_key_provider.getLast())

def validate_login_request(req):
    print(req.data)
    print(req.json)
    
    # assert(req.method == 'POST')
    # assert(req.mimetype == mimetype_json)
    # assert(req.json['user']['email'] == 'user')
    # assert(req.json['user']['password'] == 'pass')

    def generate_header_with_api_key():
        return {"api-key": api_key_provider.generateNext()}

def validate_register_request(req):
    print(req.json)
    print(req.data)
    # assert (req.method == 'POST')
    # assert (req.mimetype == mimetype_json)
    # assert (req.json['user']['email'] == 'user')
    # assert (req.json['user']['password'] == 'password')
    # assert (req.json['user']['password_confirmation'] == 'password')

def validate_logout_request(req):
    validate_api_key(req.headers['api-key'])
    print(req.json)
    print(req.data)


@app.route('/users/sign_in', methods=['GET', 'POST'])
def login():
    print('/users/sign_in')
    validate_login_request(flask.request)
    resp = flask.Response(
        json.dumps(login_response),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp

@app.route('/users/sign_up', methods=['GET', 'POST'])
def register():
    print('/users/sign_up')
    validate_register_request(flask.request)
    return flask.Response(json.dumps(register_response), mimetype=mimetype_json)

@app.route('/users/sign_out', methods=['DELETE'])
def logout():
    print('/users/sign_out')
    validate_logout_request(flask.request)
    return flask.Response(mimetype=mimetype_json)

def validate_flats_request(req):
    validate_api_key(req.headers['api-key'])

@app.route('/accessories', methods=["POST"])
def accessories():
    print('/accessories')
    resp = deepcopy(flatAccessoriesResp)
    req = flask.request
    room_type = req.json["room_type"]
    resp["data"]["accessories"] = accessories_by_room_type[room_type]
    resp = flask.Response(
        json.dumps(resp),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp

@app.route('/accessories/<path:id>/accessory_descriptions', methods=["GET"])
def accessory_descriptions(id):
    print('/accessories/{}/accessory_descriptions', id)
    resp = flask.Response(
        json.dumps(accessoryDescriptionsResp),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp

@app.route('/flats', methods=["GET"])
def flats():
    print('/flats')
    validate_flats_request(flask.request)
    resp = flask.Response(
        json.dumps(flats_response),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp


@app.route('/flats', methods=["POST"])
def flats_post():
    print('/flats [POST]')
    # validate_flats_request(flask.request)
    resp = flask.Response(
        json.dumps(flats_post_response),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp

@app.route('/flats/<path:id>', methods=["GET"])
def flatById(id):
    print("flatById: ", id)
    validate_api_key(flask.request.headers['api-key'])
    resp = flask.Response(
        json.dumps(generateFlatById(id)),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp


@app.route('/flats/<path:id>/described_accessories', methods=["GET"])
def flats_described_accessoriesById(id):
    print("[GET] flats_described_accessories_by_id: ", id)
    validate_api_key(flask.request.headers['api-key'])
    resp = flask.Response(
        json.dumps(generateFlatById(id)),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp

def validate_flats_described_accessoriesById_request(req):
    print("accessory_id", req.json["described_accessory"]["accessory_id"])
    print("accessory_description_id", req.json["described_accessory"]["accessory_description_id"])
    print("description", req.json["described_accessory"]["description"])


def generate_flats_described_accessoriesById_response(req):
    resp = {
        "success": True,
        "data": req.json,
        "error":[]
    }
    return resp

@app.route('/flats/<path:id>/described_accessories', methods=["POST"])
def flats_described_accessoriesById_post(id):
    print("[POST} flats_described_accessories_by_id: ", id)
    validate_api_key(flask.request.headers['api-key'])
    validate_flats_described_accessoriesById_request(flask.request)
    respJson = generate_flats_described_accessoriesById_response(flask.request)
    respJson["data"]["described_accessory"]["id"] = randint(1, 10000)
    resp = flask.Response(
        json.dumps(respJson),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp
@app.route('/flats/<path:flat_id>/rooms/<path:room_id>/described_accessories', methods=["POST"])
def rooms_described_accessoriesById_post(flat_id, room_id):
    print("[POST] rooms_described_accessoriesById_post: flatId {}, roomid {}".format(flat_id,room_id))
    validate_api_key(flask.request.headers['api-key'])
    validate_flats_described_accessoriesById_request(flask.request)
    respJson = generate_flats_described_accessoriesById_response(flask.request)
    respJson["data"]["described_accessory"]["id"] = randint(1, 10000)
    resp = flask.Response(
        json.dumps(respJson),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp


def validate_personal_data_request(req):
    print(req.json)
    print(req.data)
    validate_api_key(req.headers['api-key'])

@app.route('/users/personal_data', methods=['GET'])
def personal_data():
    print('/personal_data')
    validate_personal_data_request(flask.request)
    resp = flask.Response(
        json.dumps(personal_data_response),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp


@app.route("/users/preferences", methods=["GET"])
def preferences():
    print("/preferences")
    validate_api_key(flask.request.headers['api-key'])
    resp = flask.Response(
        json.dumps(preferencesResp),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp

def validate_change_preferences(req):
    validate_api_key(req.headers['api-key'])
    print("place = ", req.json["customer"]["place"])
    print("distance = ", req.json["customer"]["distance"])
    print("city_travel_time = ", req.json["customer"]["city_travel_time"])
    print("car_travel_time = ", req.json["customer"]["car_travel_time"])


def update_preferences(req):
    preferencesResp["data"]["customer"]["place"] = req.json["customer"]["place"]
    preferencesResp["data"]["customer"]["distance"] = req.json["customer"]["distance"]
    preferencesResp["data"]["customer"]["city_travel_time"] = req.json["customer"]["city_travel_time"]
    preferencesResp["data"]["customer"]["car_travel_time"] = req.json["customer"]["car_travel_time"]


@app.route("/users/change_preferences", methods=["PUT"])
def change_preferences():
    print("/change_preferences")
    validate_change_preferences(flask.request)
    update_preferences(flask.request)

    resp = flask.Response(
        json.dumps(preferencesResp),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp


@app.route("/building_types", methods=["GET"])
def building_types():
    print("/building_types")
    # validate_api_key(flask.request.headers['api-key'])
    resp = flask.Response(
        json.dumps(building_type_response),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp


@app.route("/windows_types", methods=["GET"])
def windows_types():
    print("/windows_types")
    # validate_api_key(flask.request.headers['api-key'])
    resp = flask.Response(
        json.dumps(windows_types_response),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp

@app.route("/room_types", methods=["GET"])
def room_type():
    validate_api_key(flask.request.headers["api-key"])
    resp = flask.Response(
        json.dumps(room_type_response),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp

def generateRoomResponseById(body, id):
    print("id {}".format(id))
    print(body["room"]["room_type"])
    print(body["room"]["yardage"])
    print(body["room"]["total_places"])
    print(body["room"]["can_rent_separately"])

    resp =  {
        "success": True,
        "data":body,
        "error":[]
    }
    resp["data"]["room"]["id"] = randint(1, 100)
    return resp


@app.route("/flats/<path:id>/rooms", methods=["POST"])
def addRoomsToFlat(id):
    print("/flat/{}/rooms".format(id))
    validate_api_key(flask.request.headers["api-key"])

    resp = flask.Response(
        json.dumps(generateRoomResponseById(flask.request.json, id)),
        mimetype=mimetype_json,
    )
    resp.headers["api-key"] = api_key_provider.generateNext()
    return resp

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=41234)
